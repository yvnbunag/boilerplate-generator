const cliSelect = require('cli-select')
const prompts = require('prompts')
const chalk = require('chalk')

/**
 * @typedef {import('@/cli-prompt').CLIPrompt} CLIPrompt
 */

/**
 * Prompt for text
 *
 * @param {string} name - Display name
 * @param {Object} [options] - Prompt options
 * @param {string} [options.value] - Initial value
 * @param {boolean} [options.required] - Require input
 * @param {() => unknown} [options.onCancel] - On cancel callback
 *
 * @return {Promise<string>} Input value
 */
 async function promptText(name, options = {}) {
  const { input } = await prompts(
    {
      type: 'text',
      name: 'input',
      message: name,
      initial: options.value || '',
      ...(
        options.required
          ? { validate: value => !value ? `${name} is required` : true }
          : {}
      )
    },
    { onCancel: options.onCancel || (() => {}) }
  )

  return input
}

/**
 * Prompt with selection from given values
 *
 * @param {CLIPrompt['Values']} values - Prompt options from
 *  keys and display from values
 *
 * @returns {Promise<string>} Selected key
 */
 async function selectPrompt(values) {
  const { id } = await cliSelect({
    values,
    defaultValue: 0,
    selected: chalk.green('(x)'),
    unselected: '( )',
    indentation: 0,
    cleanup: true,
    valueRenderer: (value, selected) => {
      if (selected) {
          return chalk.underline(value);
      }

      return value;
    },
    outputStream: process.stdout,
    // @ts-expect-error Missing typing from either expected or provided
    inputStream: process.stdin,
  })

  // @ts-expect-error suppress cliSelect numeric return value error
  return id
}

module.exports = {
  promptText,
  selectPrompt,
}
