/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'Web (Front End)',
  isDomain: true,
}

module.exports = config
