/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'export',
  devIndicators: {
    buildActivity: true,
    buildActivityPosition: 'bottom-right',
  },
  experimental: {
    typedRoutes: false,
  },
  reactStrictMode: true,
};

module.exports = nextConfig;
