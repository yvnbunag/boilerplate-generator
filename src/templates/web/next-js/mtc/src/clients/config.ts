import {
  MutationObserverOptions,
  QueryObserverOptions,
} from '@tanstack/react-query';

type ErrorResponse = {
  success: false;
  error: string;
};

export const defaultQueryOptions = {
  useErrorBoundary: true,
  refetchOnWindowFocus: false,
} satisfies QueryObserverOptions;

export const defaultMutationOptions = {
  useErrorBoundary: true,
} satisfies MutationObserverOptions;

export class ApiError extends Error {
  constructor(public message: string, public statusCode: number) {
    super(message);
    this.name = 'ApiError';
  }

  static async from(response: Response) {
    const errorResponse = (await response.json()) as ErrorResponse;

    return new ApiError(
      errorResponse.error || JSON.stringify(errorResponse, null, 2),
      response.status,
    );
  }
}
