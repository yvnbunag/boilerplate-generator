import { ApiError, defaultMutationOptions } from '@/clients/config';
import { useMutation } from '@tanstack/react-query';

const apiUrl = process.env.NEXT_PUBLIC_API_URL || '';

type Credentials = {
  username: string;
  password: string;
};

export function useLogin(
  onSuccess: (token: string) => void,
  onFail: (status: number, error: string) => void,
) {
  return useMutation({
    ...defaultMutationOptions,
    mutationFn: (credentials: Credentials) => {
      return fetch(`${apiUrl}/login`, {
        method: 'POST',
        body: JSON.stringify(credentials),
        cache: 'no-store',
      });
    },
    onSuccess: async (response) => {
      const { data, error } = await response.json();

      if (response.status < 400) {
        return onSuccess(data.token);
      }

      return onFail(response.status, error);
    },
  });
}

export function useLogout(onSuccess: () => void) {
  return useMutation({
    ...defaultMutationOptions,
    mutationFn: async () => {
      const response = await fetch(`${apiUrl}/logout`, {
        method: 'POST',
        cache: 'no-store',
      });

      if (response.status < 400) {
        return;
      }

      throw await ApiError.from(response);
    },
    onSuccess,
  });
}
