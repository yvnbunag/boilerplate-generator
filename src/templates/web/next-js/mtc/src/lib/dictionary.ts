import en from '@/dictionaries/en.json';
import { useMemo } from 'react';

type Dictionary = typeof en;

export function getDictionary(): Dictionary {
  return en;
}

export function useDictionary(): Dictionary {
  return useMemo(getDictionary, []);
}

export function substitute(
  template: string,
  values: Record<string, unknown>,
): string {
  let replaced = template;

  for (const key in values) {
    replaced = replaced.replace(`{${key}}`, String(values[key]));
  }

  return replaced;
}
