import { formatCurrency, formatDate, formatDateTime } from '@/lib/format';
import { describe, expect, it } from 'vitest';

describe('formatCurrency', () => {
  it('should format currency', () => {
    expect(formatCurrency(0)).toEqual('$0.00');
    expect(formatCurrency(-1)).toEqual('-$1.00');
    expect(formatCurrency(1)).toEqual('$1.00');
    expect(formatCurrency(10)).toEqual('$10.00');
    expect(formatCurrency(100)).toEqual('$100.00');
    expect(formatCurrency(1000)).toEqual('$1,000.00');
  });
});

describe('formatDate', () => {
  it('should format date', () => {
    expect(formatDate(new Date('01-01-2023'))).toEqual('January 1, 2023');
  });
});

describe('formatDateTime', () => {
  it('should format date with time', () => {
    expect(formatDateTime(new Date('01-01-2023 13:00'))).toEqual(
      'January 1, 2023 at 01:00 PM',
    );
  });
});
