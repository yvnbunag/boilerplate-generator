'use client';

import { useDictionary } from '@/lib/dictionary';
import { AnalyticsIcon } from '@/lib/icons';
import { Typography } from '@mui/material';

type LogoProps = { href: string; 'data-testid'?: string };
export function Logo(logoProps: LogoProps) {
  const dict = useDictionary();

  return (
    <Typography
      data-testid={logoProps['data-testid']}
      variant='h6'
      noWrap
      component='a'
      href={logoProps.href}
      sx={{
        mr: 2,
        display: 'flex',
        alignItems: 'center',
        fontFamily: 'monospace',
        fontWeight: 700,
        letterSpacing: '.05rem',
        color: 'primary.contrastText',
        textDecoration: 'none',
      }}>
      <AnalyticsIcon sx={{ mr: 1 }} />
      {dict.appName}
    </Typography>
  );
}
