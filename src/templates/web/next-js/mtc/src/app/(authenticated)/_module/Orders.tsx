'use client';

import { FillLoader } from '@/lib/components/FillLoader';
import { substitute, useDictionary } from '@/lib/dictionary';
import { formatCurrency, formatDateTime } from '@/lib/format';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@mui/material';
import { useMemo } from 'react';

type OrderData = {
  id: number;
  date: number;
  name: string;
  shipTo: string;
  paymentMethod: {
    provider: string;
    lastFourDigits: string;
  };
  amount: number;
};
type OrdersProps = {
  data: OrderData[];
  loading: boolean;
};
export function Orders({ data, loading }: OrdersProps) {
  const dict = useDictionary();
  const rows = useMemo(() => {
    return data.map((row) => ({
      ...row,
      date: formatDateTime(new Date(row.date)),
      amount: formatCurrency(row.amount),
    }));
  }, [data]);

  if (loading) {
    return <FillLoader />;
  }

  return (
    <Table size='small'>
      <TableHead>
        <TableRow>
          <TableCell>{dict.landing.orders.table.headers.orderedOn}</TableCell>
          <TableCell>{dict.landing.orders.table.headers.name}</TableCell>
          <TableCell>{dict.landing.orders.table.headers.shipTo}</TableCell>
          <TableCell>
            {dict.landing.orders.table.headers.paymentMethod}
          </TableCell>
          <TableCell align='right'>
            {dict.landing.orders.table.headers.amount}
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow key={row.id}>
            <TableCell>{row.date}</TableCell>
            <TableCell>{row.name}</TableCell>
            <TableCell>{row.shipTo}</TableCell>
            <TableCell>
              {substitute(dict.landing.orders.table.rows.paymentMethod, {
                provider: row.paymentMethod.provider,
                lastFourDigits: row.paymentMethod.lastFourDigits,
              })}
            </TableCell>
            <TableCell align='right'>{row.amount}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
