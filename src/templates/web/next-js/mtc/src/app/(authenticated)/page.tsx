'use client';

import { Deposits } from '@/app/(authenticated)/_module/Deposits';
import { Orders } from '@/app/(authenticated)/_module/Orders';
import { Sales } from '@/app/(authenticated)/_module/Sales';
import { Title } from '@/app/(authenticated)/_module/Title';
import { useDeposits, useOrders, useSales } from '@/clients/api';
import { Fillable } from '@/lib/components/Fillable';
import { useDictionary } from '@/lib/dictionary';
import { Box, Container, Grid, Pagination, Paper } from '@mui/material';
import { useState } from 'react';

export default function Page() {
  const dict = useDictionary();
  const [today] = useState(new Date());
  const sales = useSales({ date: today });
  const deposits = useDeposits({ date: today });
  const orders = useOrders({ date: today });

  return (
    <Container maxWidth='lg' sx={{ mt: 4, mb: 4 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <Fillable
            data-testid='landing-card-sales'
            component={Paper}
            padding={2}
            minHeight={275}>
            <Title>{dict.landing.sales.heading}</Title>
            <Sales sales={sales.data.sales} loading={sales.loading} />
          </Fillable>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Fillable
            data-testid='landing-card-deposits'
            component={Paper}
            padding={2}
            minHeight={275}>
            <Title>{dict.landing.deposits.heading}</Title>
            <Deposits
              deposits={deposits.data.deposits}
              date={deposits.data.date}
              loading={deposits.loading}
            />
          </Fillable>
        </Grid>
        <Grid item xs={12}>
          <Fillable
            data-testid='landing-card-orders'
            component={Paper}
            padding={2}
            minHeight={450}>
            <Box display='flex' justifyContent='space-between'>
              <Title>{dict.landing.orders.heading}</Title>
              <Pagination
                page={orders.page}
                count={orders.pages}
                onChange={(_, page) => orders.setPage(page)}
              />
            </Box>
            <Orders data={orders.data.orders} loading={orders.loading} />
          </Fillable>
        </Grid>
      </Grid>
    </Container>
  );
}
