'use client';

import { Typography } from '@mui/material';

export function Title({ children }: React.PropsWithChildren) {
  return (
    <Typography component='h2' variant='h6' color='primary' gutterBottom>
      {children}
    </Typography>
  );
}
