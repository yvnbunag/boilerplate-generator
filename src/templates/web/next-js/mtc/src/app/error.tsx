'use client';

import { ApiError } from '@/clients/config';
import { Fillable } from '@/lib/components/Fillable';
import { useDictionary } from '@/lib/dictionary';
import { Box, Button, Typography } from '@mui/material';
import Link from 'next/link';
import { useEffect } from 'react';

export default function Error({
  error,
  reset,
}: {
  error: Error;
  reset: () => void;
}) {
  const dict = useDictionary();

  useEffect(() => {
    console.error(error);
  }, [error]);

  if (error instanceof ApiError) {
    return <ErrorMessage statusCode={error.statusCode} reset={reset} />;
  }

  return <ErrorMessage reset={reset} />;
}

type ErrorMessageProps = {
  reset: () => void;
  statusCode?: string | number;
};
function ErrorMessage({ reset, statusCode = 500 }: ErrorMessageProps) {
  const dict = useDictionary();

  return (
    <Fillable alignItems='center' justifyContent='center' fill>
      <Typography variant='h1'>{statusCode}</Typography>
      <Typography variant='body1' marginBottom={4}>
        {dict.error.body}
      </Typography>
      <Box display='flex' gap={1}>
        <Link href='/'>
          <Button variant='outlined'>{dict.error.button.goToHome}</Button>
        </Link>
        <Button variant='contained' onClick={reset}>
          {dict.error.button.reload}
        </Button>
      </Box>
    </Fillable>
  );
}
