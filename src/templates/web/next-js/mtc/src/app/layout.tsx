import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import { RootLayout } from '@/app/_module/RootLayout';
import { FillableBody } from '@/lib/components/Fillable';
import { MockApi } from '@/lib/mockApi';
import { Metadata } from 'next';

export const metadata: Metadata = {
  title: {
    default: '<package-name>',
    template: `%s | <package-name>`,
  },
};

export default function Layout({ children }: React.PropsWithChildren) {
  return (
    <html>
      <FillableBody>
        <MockApi />
        <RootLayout>{children}</RootLayout>
      </FillableBody>
    </html>
  );
}
