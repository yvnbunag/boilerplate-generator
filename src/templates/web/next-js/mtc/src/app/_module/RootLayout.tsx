'use client';

import { CssBaseline, ThemeProvider, createTheme } from '@mui/material';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

/**
 * Palette https://github.com/ianbunag/ianbunag.github.io
 * Theme builder https://mui.com/material-ui/customization/theming/#theme-builder
 */
const theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: '#254e58',
      dark: '#112d32',
      light: '#254e58',
      contrastText: '#fff',
    },
    secondary: {
      main: '#6e6658',
      dark: '#4f4a41',
      light: '#6e6658',
      contrastText: '#fff',
    },
  },
});
const queryClient = new QueryClient();

export function RootLayout({ children }: React.PropsWithChildren) {
  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <CssBaseline>{children}</CssBaseline>
      </ThemeProvider>
    </QueryClientProvider>
  );
}
