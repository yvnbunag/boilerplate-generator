'use client';

import { LoadingButton } from '@mui/lab';
import {
  Alert,
  Avatar,
  Box,
  Container,
  IconButton,
  Paper,
  Stack,
  TextField,
  Typography,
} from '@mui/material';

import { useLogin } from '@/clients/auth';
import {
  EMPTY_STRING,
  notEmpty,
  useField,
  validEmail,
} from '@/lib/hooks/useField';

import { useDictionary } from '@/lib/dictionary';
import { CloseIcon, LockPersonIcon, LoginIcon } from '@/lib/icons';
import { useRouter } from 'next/navigation';
import { useCallback, useState } from 'react';

export default function Page() {
  const dict = useDictionary();
  const username = useField('Username', EMPTY_STRING, [notEmpty, validEmail]);
  const password = useField('Password', EMPTY_STRING, [notEmpty]);
  const router = useRouter();
  const [loginError, setLoginError] = useState('');
  const login = useLogin(
    (token) => {
      console.info(`token: ${token}`);
      router.push('/');
    },
    (status, error) => {
      if (status === 401) {
        return setLoginError(dict.login.error.incorrectCredentials);
      }

      console.error(error);
      setLoginError(dict.login.error.internalServerError);
    },
  );
  const validate = useCallback(() => {
    let isValid = true;

    username.validate() || (isValid = false);
    password.validate() || (isValid = false);

    return isValid;
  }, [password, username]);

  return (
    <Container maxWidth='xs'>
      <LoginCard
        onSubmit={() => {
          validate() &&
            login.mutate({
              username: username.value,
              password: password.value,
            });
        }}>
        <Avatar sx={{ bgcolor: 'secondary.main' }}>
          <LockPersonIcon />
        </Avatar>

        <Typography component='h1' variant='h5'>
          {dict.login.heading}
        </Typography>

        {loginError ? (
          <Alert
            severity='error'
            sx={{ width: '100%' }}
            action={
              <IconButton
                aria-label='close'
                size='small'
                onClick={() => setLoginError('')}>
                <CloseIcon fontSize='inherit' />
              </IconButton>
            }>
            {loginError}
          </Alert>
        ) : null}

        <TextField
          data-testid='login-input-username'
          type='email'
          label={dict.login.input.username}
          variant='outlined'
          autoComplete='email'
          disabled={login.isLoading}
          value={username.value}
          onChange={(event) => username.setValue(event.target.value)}
          error={username.hasError}
          helperText={username.errorMessage}
          fullWidth
          required
        />

        <TextField
          data-testid='login-input-password'
          type='password'
          label={dict.login.input.password}
          variant='outlined'
          autoComplete='current-password'
          disabled={login.isLoading}
          value={password.value}
          onChange={(event) => password.setValue(event.target.value)}
          error={password.hasError}
          helperText={password.errorMessage}
          fullWidth
          required
        />

        <LoadingButton
          data-testid='login-button-login'
          variant='contained'
          type='submit'
          loading={login.isLoading}
          loadingPosition='end'
          endIcon={<LoginIcon />}
          fullWidth>
          {dict.login.button.login}
        </LoadingButton>
      </LoginCard>
    </Container>
  );
}

type LoginCardProps = {
  onSubmit: () => void;
};
function LoginCard({
  children,
  onSubmit,
}: React.PropsWithChildren<LoginCardProps>) {
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        onSubmit();
      }}
      noValidate>
      <Paper
        variant='elevation'
        elevation={8}
        component={Box}
        marginTop={8}
        padding={2}>
        <Stack alignItems='center' spacing={2} width='100%'>
          {children}
        </Stack>
      </Paper>
    </form>
  );
}
