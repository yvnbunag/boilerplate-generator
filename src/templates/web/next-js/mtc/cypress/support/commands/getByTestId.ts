Cypress.Commands.add('getByTestId', (testId) => {
  return cy.get(`[data-testid="${testId}"]`);
});

declare global {
  namespace Cypress {
    interface Chainable<Subject> {
      /**
       * @example cy.getByTestId('login-input-username');
       */
      getByTestId(testId: string): Chainable<Subject>;
    }
  }
}

export {};
