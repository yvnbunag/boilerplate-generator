import { defineConfig } from 'cypress';

module.exports = defineConfig({
  fixturesFolder: false,
  trashAssetsBeforeRuns: true,
  retries: 1,
  defaultCommandTimeout: 10000,
  experimentalMemoryManagement: true,
  e2e: {
    baseUrl: 'http://localhost:3000',
    experimentalRunAllSpecs: true,
    testIsolation: true,
  },
});
