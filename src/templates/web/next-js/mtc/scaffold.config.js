const { standardReplacements } = require('~/config');

/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'MTC (Material UI, TypeScript and Cypress)',
  successMessages: ['Verify package.json before starting development'],
  replacements: standardReplacements,
};

module.exports = config;
