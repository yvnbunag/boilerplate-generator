/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'Fastify',
  isDomain: true,
}

module.exports = config
