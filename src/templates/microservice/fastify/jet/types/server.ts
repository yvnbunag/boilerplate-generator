import type * as models from '~/models'

export type ENV = Partial<Record<
  'NODE_ENV'
  | 'PORT'
  | 'BASIC_AUTH_USERNAME'
  | 'BASIC_AUTH_PASSWORD'
  | 'TEST_DATABASE',
  string
>>

export interface Config {
  version: string,
  port: number,
}

export interface Dependencies {
  config: Config,
  models: ReturnType<typeof models.create>,
}
