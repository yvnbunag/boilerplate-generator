import { HTTPError } from '@/fastify/errors/http'

export class BadRequestError extends HTTPError {
  constructor(message = 'bad request') {
    super(message, 400)

    this.name = 'BadRequestError'
  }
}
