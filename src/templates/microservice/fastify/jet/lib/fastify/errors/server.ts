import { HTTPError } from '@/fastify/errors/http'

export class ServerError extends HTTPError {
  constructor(message = 'server error') {
    super(message, 500)

    this.name = 'ServerError'
  }
}
