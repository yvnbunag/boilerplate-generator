import { HTTPError } from '@/fastify/errors/http'

export class UnauthorizedError extends HTTPError {
  constructor(message = 'unauthorized') {
    super(message, 401)

    this.name = 'UnauthorizedError'
  }
}
