import { handleHTTPError } from '@/fastify/error-handler/handle-http-error'
import { handleValidationError } from '@/fastify/error-handler/handle-validation-error'

import type { FastifyReply } from 'fastify'
import type { Composite, ErrorHandler } from '@/fastify/types'

interface Payload {
  error: string,
  message: string,
}

type ReplyWithError = (
  reply: FastifyReply,
  statusCode: number,
  payload: Payload
)=> void

export const defaultReplyWithError: ReplyWithError
  = (reply, statusCode, payload) => reply.status(statusCode).send(payload)

const handleUncaughtError: ErrorHandler.Handler = function (error, handle) {
  handle()
}

export function create(
  handlers: Array<ErrorHandler.Handler> = [],
  replyWithError: ReplyWithError = defaultReplyWithError,
): Composite {
  const injectedHandlers = [...handlers, handleUncaughtError]

  return function registerErrorHandler (app) {
    return app.setErrorHandler((error, request, reply) => {
      let handled = false

      const handleCallback: ErrorHandler.Handle = function (response = {}) {
        const { statusCode = 500 } = response
        const payload: Payload = {
          error: response.error || error.name || 'Error',
          message: response.message || error.message || '',
        }

        replyWithError(reply, statusCode, payload)
        handled = true
      }

      for (const handle of injectedHandlers) {
        if (handled) break

        handle(error, handleCallback)
      }
    })
  }
}

export {
  handleHTTPError,
  handleValidationError,
}
