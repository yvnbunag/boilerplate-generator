import { HTTPError } from '@/fastify/errors/http'

import type { ErrorHandler } from '@/fastify/types'

export const handleHTTPError: ErrorHandler.Handler = function (error, handle) {
  if (!(error instanceof HTTPError)) return

  handle({
    statusCode: error.statusCode,
    error: error.name,
    message: error.message,
  })
}
