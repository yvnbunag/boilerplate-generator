import type { ErrorHandler } from '@/fastify/types'

/**
 * Handle AJV validation error
 */
export const handleValidationError: ErrorHandler.Handler
  = function(error, handle) {
    if (!isValidationError(error)) return

    handle({
      statusCode: 400,
      error: error.name === 'Error' ? 'ValidationError' : error.name,
      message: error.message,
    })
  }

function isValidationError(error: Error): boolean {
  if ('validation' in error && 'validationContext' in error) return true

  return false
}