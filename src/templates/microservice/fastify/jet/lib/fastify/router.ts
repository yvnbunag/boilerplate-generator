import type { Route, Composite } from '@/fastify/types'

export function create(routes: Array<Route.Module>): Composite {
  return function registerRouter (app) {
    for (const { register, options } of routes) app.register(register, options)

    return app
  }
}
