import { NotFoundError } from '@/fastify/errors'

import type { FastifyRequest } from 'fastify'
import type { Composite } from '@/fastify/types'

export const composite: Composite = function registerInterceptNotFound(app) {
  return app.setNotFoundHandler(interceptNotFound)
}

function interceptNotFound(request: FastifyRequest) {
  const { method, url } = request.raw

  throw new NotFoundError(`Route ${method}:${url} not found`)
}
