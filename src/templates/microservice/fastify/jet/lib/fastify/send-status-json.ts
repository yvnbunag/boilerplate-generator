import type { FastifyReply } from 'fastify'
import type { Composite } from '@/fastify/types'

export const composite: Composite = function registerSendStatusJSON(app) {
  return app.decorateReply('sendStatusJSON', sendStatusJSON)
}

export function sendStatusJSON(this: FastifyReply, data?: unknown) {
  const { statusCode } = this.raw
  const success = statusCode >= 200 && statusCode < 400
  const additionalData = data !== undefined ? { data } : {}

  this
    .header('Content-Type', 'application/json; charset=utf-8')
    .send({ success, ...additionalData })
}
