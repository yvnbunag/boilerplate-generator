import type { Sequelize } from 'sequelize'
import type { Connection } from '@/sequelize/types'

export class Locator<Key extends string> {
  private _cache: Partial<Record<Key, Sequelize>> = {}

  constructor(private readonly factory: Connection.Factory<Key>) {}

  get(key: Key): Sequelize {
    if (!this._cache[key]) {
      const connection = this.factory[key]()

      this._cache[key] = connection
    }

    return this._cache[key] as Sequelize
  }
}
