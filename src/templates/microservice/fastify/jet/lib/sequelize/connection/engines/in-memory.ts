import { Sequelize } from 'sequelize'

import type { Connection } from '@/sequelize/types'

export const create: Connection.CreateInitialize<
  Connection.DefaultOptions
> = (options = {}) => {
  return function initializeInMemoryConnection() {
    return new Sequelize('sqlite::memory:', options)
  }
}
