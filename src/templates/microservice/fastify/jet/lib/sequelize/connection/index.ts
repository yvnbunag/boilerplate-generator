import { merge } from 'lodash'
import * as engines from '@/sequelize/connection/engines'
import type { Connection } from '@/sequelize/types'

export { Locator } from '@/sequelize/connection/locator'

export enum Engine {
  IN_MEMORY,
  SQLITE,
  MYSQL,
}

const defaultOptions: Connection.DefaultOptions = {
  logging: false,
}
const initializers = {
  [Engine.IN_MEMORY]: engines.inMemory.create,
  [Engine.SQLITE]: engines.sqlite.create,
  [Engine.MYSQL]: engines.mysql.create,
} as const

export function initialize<
  ProvidedEngine extends Engine,
  Options extends Parameters<typeof initializers[ProvidedEngine]>[0],
>(
  ...args: Partial<Options> extends Options
    ? [ProvidedEngine, Options?]
    : [ProvidedEngine, Options]
): Connection.Initialize {
  const [engine, options = {} as Options] = args
  const computedOptions = merge(defaultOptions, options)

  return initializers[engine](computedOptions as never)
}
