import type { Sequelize } from 'sequelize'
import type { connection } from '@/sequelize'
import type { Model } from '@/sequelize/types'

export class Locator<
  Key extends string = string,
  ConnectionKey extends string = string,
  Factory extends Model.Factory<
    Key,
    ConnectionKey
  > = Model.Factory<Key, ConnectionKey>,
> {
  private _cache: Partial<Record<keyof Factory, unknown>> = {}

  constructor(
    private readonly connectionLocator: connection.Locator<ConnectionKey>,
    private readonly factory: Factory,
  ) {}

  get<
    Key extends keyof Factory,
    Model extends ReturnType<Factory[Key]['initialize']> & Sequelize,
  >(key: Key): Model {
    if (!this._cache[key]) {
      const { initialize, connection } = this.factory[key]
      const sequelize = this.connectionLocator.get(connection)
      const model = initialize(sequelize)

      this._cache[key] = model
    }

    return this._cache[key] as Model
  }

  getAll<
    Key extends keyof Factory,
    Model extends ReturnType<Factory[Key]['initialize']> & Sequelize,
  >(): Record<Key, Model> {
    const keys = Object.keys(this.factory) as Array<Key>
    const models = keys.map((key) => [key, this.get(key)])

    return Object.fromEntries(models)
  }
}
