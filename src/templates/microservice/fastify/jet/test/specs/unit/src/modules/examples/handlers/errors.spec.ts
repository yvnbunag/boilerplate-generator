import {
  throwHTTPError,
  errorMap,
} from '~/modules/examples/handlers/errors'
import { suppress } from '$/utils/invocations'


describe('Error is mapped', () => {
  const serverError = Object
    .keys(errorMap)
    .find((key) => key.indexOf('server') > -1)
    || ''

  it('should throw error with default message if not provided', () => {
    expect(suppress(() => throwHTTPError(serverError))).toMatchSnapshot()
  })

  it('should throw error with message if provided', () => {
    const error = suppress(() => throwHTTPError(serverError, 'custom-message'))

    expect(error).toMatchSnapshot()
  })
})

it('should throw NotFoundError if error is not mapped', () => {
  expect(suppress(() => throwHTTPError('not-mapped'))).toMatchSnapshot()
})
