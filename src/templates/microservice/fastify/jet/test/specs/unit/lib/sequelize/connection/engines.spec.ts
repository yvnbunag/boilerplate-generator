import { Sequelize } from 'sequelize'
import { inMemory, sqlite, mysql } from '@/sequelize/connection/engines'
import { mapFunctionName } from '$/utils/transformers'
import { getFirstArguments } from '$/utils/mocks'

jest.mock('sequelize')

describe('In memory', () => {
  describe('create', () => {
    const initializeConnection = inMemory.create({ logging: false })

    beforeAll(jest.clearAllMocks)

    it('should return connection factory', () => {
      expect(typeof initializeConnection).toBe('function')
      expect(mapFunctionName(initializeConnection)).toMatchSnapshot()
    })

    describe('Connection factory', () => {
      it('should return connection', () => {
        expect(initializeConnection()).toBeInstanceOf(Sequelize)
        expect(getFirstArguments(Sequelize as never)).toMatchSnapshot()
      })
    })
  })
})

describe('SQLite', () => {
  describe('create', () => {
    const initializeConnection = sqlite.create({ storage: '/tmp/test.sqlite' })

    beforeAll(jest.clearAllMocks)

    it('should return connection factory', () => {
      expect(typeof initializeConnection).toBe('function')
      expect(mapFunctionName(initializeConnection)).toMatchSnapshot()
    })

    describe('Connection factory', () => {
      it('should return connection', () => {
        expect(initializeConnection()).toBeInstanceOf(Sequelize)
        expect(getFirstArguments(Sequelize as never)).toMatchSnapshot()
      })
    })
  })
})

describe('MySQL', () => {
  describe('create', () => {
    const initializeConnection = mysql.create({
      database: 'test-database',
      username: 'test-username',
      password: 'test-password',
    })

    beforeAll(jest.clearAllMocks)

    it('should return connection factory', () => {
      expect(typeof initializeConnection).toBe('function')
      expect(mapFunctionName(initializeConnection)).toMatchSnapshot()
    })

    describe('Connection factory', () => {
      it('should return connection', () => {
        expect(initializeConnection()).toBeInstanceOf(Sequelize)
        expect(getFirstArguments(Sequelize as never)).toMatchSnapshot()
      })
    })
  })
})
