import { start } from '~/server'
import { server } from '@/fastify'
import { getFirstArguments } from '$/utils/mocks'
import { mapFunctionName } from '$/utils/transformers'

import type { FastifyInstance } from 'fastify'

jest.mock('+/package.json', () => ({ version: '0.0.1' }))

describe('start', () => {
  it('should start server', () => {
    const ready = jest.fn()
    const dependencies = { config: { port: 3010 } }
    const app = { ready, dependencies } as unknown as FastifyInstance
    const spies = {
      create: jest.spyOn(server, 'create').mockReturnValueOnce(app),
      serve: jest.spyOn(server, 'serve').mockImplementationOnce((app) => app),
    }

    start({ PORT: '3010' })
    const [readyCallback] = getFirstArguments(ready)
    if (typeof readyCallback === 'function') readyCallback()
    const serveArguments = getFirstArguments(spies.serve)

    expect(spies.create).toHaveBeenCalledTimes(1)
    expect(ready).toHaveBeenCalledTimes(1)
    expect(spies.serve).toHaveBeenCalledTimes(1)
    expect(mapFunctionName(getFirstArguments(spies.create))).toMatchSnapshot()
    expect(serveArguments[0]).toBe(app)
    expect(serveArguments[1]).toMatchInlineSnapshot(`3010`)
  })
})
