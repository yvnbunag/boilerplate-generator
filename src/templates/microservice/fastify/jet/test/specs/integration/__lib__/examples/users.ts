import sequelizeFixtures from 'sequelize-fixtures'

import type { FastifyInstance } from 'fastify'
import type { Note } from '~/models/note'

export const notes = {
  fixtures: (() => {
    const model = 'Note'

    type Attributes = Partial<Note.RawAttributes>

    interface Fixture {
      model: typeof model,
      data: Attributes,
    }

    type Fixtures = Array<Fixture>

    interface Options {
      from?: number,
      to?: number,
      seed?: string,
    }

    function create(
      override: Attributes = {},
      { from = 1, to = 1, seed = '' }: Options = {},
    ): Fixtures {
      const start = from - 1
      const length = to > from ? to - start : 1
      const keys: Array<number> = Array.from(
        { length },
        (value, index) => index + start + 1,
      )

      return keys.map((key) => {
        const identifier = seed ? `${seed} ${key}` : key

        return {
          model,
          data: {
            title: `Title ${identifier}`,
            note: `Note ${identifier}`,
            userID: 1,
            ...override,
          },
        }
      })
    }

    function createFromArray(
      createArguments: Array<[Attributes?, Options?]>,
    ): Fixtures {
      return createArguments
        .flatMap(([override, options]) => create(override, options))
    }

    async function load(
      fixtures: Fixtures, source: { [model]: typeof Note},
    ): Promise<Array<Readonly<Note>>> {
      const { models } = await sequelizeFixtures.loadFixtures(fixtures, source)

      return models[model]
    }

    return {
      create,
      createFromArray,
      load,
    }
  })(),
  helpers: (() => {
    function getAll(
      app: FastifyInstance,
      userID: number,
      query?: { page?: string, limit?: string },
    ) {
      return app.inject({
        method: 'GET',
        url: `/examples/users/${userID}/notes`,
        query,
      })
    }

    function create(
      app: FastifyInstance,
      userID: number,
      payload?: Record<string, unknown>,
    ) {
      return app.inject({
        method: 'POST',
        url: `/examples/users/${userID}/notes`,
        payload,
      })
    }

    function get(app: FastifyInstance, userID: number, noteID: number) {
      return app.inject({
        method: 'GET',
        url: `/examples/users/${userID}/notes/${noteID}`,
      })
    }

    function update(
      app: FastifyInstance,
      userID: number,
      noteID: number,
      payload?: Record<string, unknown>,
    ) {
      return app.inject({
        method: 'PATCH',
        url: `/examples/users/${userID}/notes/${noteID}`,
        payload,
      })
    }

    function destroy(app: FastifyInstance, userID: number, noteID: number) {
      return app.inject({
        method: 'DELETE',
        url: `/examples/users/${userID}/notes/${noteID}`,
      })
    }

    return {
      getAll,
      create,
      get,
      update,
      destroy,
    }
  })(),
  matchers: {
    default: {
      id: expect.any(Number),
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
    },
  },
}
