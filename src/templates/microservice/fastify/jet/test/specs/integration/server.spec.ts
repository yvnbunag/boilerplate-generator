import { defaultComposition } from '~/server'
import { server, router } from '@/fastify'

import type { Route } from '@/fastify/types'
import type { Dependencies } from '#/server'

const errorRoute: Route.Module = {
  register(app, options, done) {
    app.get('/unhandled-error', () => {
      throw new Error('unhandled-error')
    })

    done()
  },
}
const app = server.create<Dependencies>([
  router.create([errorRoute]),
  ...defaultComposition,
])

it('should handle route not found errors', async () => {
  const response = await app.inject({
    method: 'GET',
    path: '/non-existent-route',
  })

  expect(response.json()).toMatchSnapshot()
  expect(response.statusCode).toMatchInlineSnapshot(`404`)
})

it('should handle unhandled errors', async () => {
  const response = await app.inject({
    method: 'GET',
    path: '/unhandled-error',
  })

  expect(response.json()).toMatchSnapshot()
  expect(response.statusCode).toMatchInlineSnapshot(`500`)
})
