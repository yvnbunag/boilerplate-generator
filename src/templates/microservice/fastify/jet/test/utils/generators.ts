export function createCounter(start = 0): ()=> number {
  const generator = (function* generator() { while (true) yield start++ })()

  return () => generator.next().value
}
