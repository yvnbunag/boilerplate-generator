export function mapFunctionName<Value>(value: Value): Value {
  const stringified = JSON.stringify(
    value,
    (key, value) => typeof value === 'function' ? `${value.name}()` : value,
  )

  return JSON.parse(stringified)
}
