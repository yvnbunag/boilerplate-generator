export function suppress<
  Type extends Method,
>(method: Type): Error | ReturnType<Type> {
  try {
    return method()
  } catch (error) {
    return error as Error
  }
}
