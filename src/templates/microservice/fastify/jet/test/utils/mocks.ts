/**
 * Any jest.MockInstance extender (jest.Mock, jest.SpyInstance)
 */
export type Mock<T = any, Y extends Array<any> = any> = jest.MockInstance<T, Y> // eslint-disable-line @typescript-eslint/no-explicit-any

export function getFirstArguments(mock: Mock): Array<unknown> {
  return mock.mock.calls.slice(0, 1).shift()
}
