# <package-name>

Microservice project with Fastify, powered by Jest, ESLint and TypeScript

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Scaffold setup](#scaffold-setup)
- [Usage](#usage)
  - [Running tests](#running-tests)
  - [Setting up environment variables](#setting-up-environment-variables)
  - [Setting up database](#setting-up-database)
  - [Structure](#structure)
  - [API](#api)
  - [Development](#development)
  - [Production](#production)
  - [Linting code](#linting-code)
- [Scaffold](#scaffold)

<br/>

## Requirements

1. [Node.js](https://nodejs.org/en/) version 16.11.1 or higher
2. [pnpm](https://pnpm.io/) version 6.11.0 or higher
   - May be swapped with other package managers such as
  [npm](https://docs.npmjs.com/) and [yarn](https://yarnpkg.com/)

<br/>

## Scaffold setup

1. Initialize git repository

```sh
git init
git add .
```

2. Install dependencies

```sh
pnpm install
```

3. Commit and push project to remote repository

```sh
git commit -m "<commit-message>"
git remote add origin <remote-origin>
git push --set-upstream origin <branch-name>
```

<br/>

## Usage

### Running tests

```sh
# Specific test specs
pnpm test:focused test/specs/integration/examples.spec.ts

# All
pnpm test
```

### Setting up environment variables

Duplicate environment file from template

```sh
cp .env.dist .env
```

### Setting up database

Sync [Sequelize](https://sequelize.org/) models defined in
[src/models](src/models) directory

```sh
pnpm database:sync
```

> By default, the sync destination is an SQLite database, to be stored in
  [storage](storage) directory. This may be configured in
  [src/server.ts](src/server.ts)

### Structure

- [src](./src) - Source code directory
  - [src/index.ts](./src/index.ts) - Server startup entry point
  - [src/server.ts](./src/server.ts)
    - Application is defined here via composition
    - Where global dependencies are injected to the application instance
- [lib](./lib) - Reusable components directory
- [storage](./storage) - Default SQLite database files directory
- [test](./test) - Test directory
  - [test/setup](./test/setup) - Setup and teardown definitions directory
  - [test/specs](./test/specs) - Specifications directory, base contains
    [integration](./test/specs/integration) and [unit](./test/specs/unit) tests
  - [test/utils](./test/utils) - Reusable utilities directory
- [types](./types) - Global types directory
  - [types/fastify.d.ts](./types/fastify.d.ts) - Module augmentation definition
    of application instance dependency injections and decorations
  - [types/server.ts](./types/server.ts) - Application config and dependencies
    definition

### API

See [Postman collection](./fastify-jet.postman-collection.json), which may be
imported into the
[Postman](https://www.postman.com/product/what-is-postman/) application for
inspection

### Development

Start service in watched mode

```sh
pnpm dev
```

### Production

Build production package

```sh
pnpm build
```

Start service from production package

```sh
pnpm start
```

### Linting code

```sh
# Lint only
pnpm lint

# With auto fix
pnpm lint:fix
```

## Scaffold

Project scaffolded with [@ianbunag/scaffold](https://www.npmjs.com/package/@ianbunag/scaffold)
