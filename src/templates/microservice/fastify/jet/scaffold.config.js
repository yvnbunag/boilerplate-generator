const { standardReplacements } = require('~/config')

/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: "JET (Jest, ESLint and TypeScript)",
  successMessages: [
    'Verify package.json before starting development',
  ],
  replacements: standardReplacements,
}

module.exports = config
