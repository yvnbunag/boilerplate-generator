import { createModels } from '~/server'
import { sync } from '~/models'

async function syncDatabase(env: NodeJS.ProcessEnv) {
  await sync(createModels(env))
}

syncDatabase(process.env)
