import * as controllers from '~/controllers/root'

import type { Route } from '@/fastify/types'

export const register: Route.Register = (app, options, done) => {
  app.get(
    '/version',
    controllers.getVersion,
  )

  done()
}

export const options: Route.Options = {
  prefix: '/',
}
