import * as controllers from '~/controllers/examples'
import * as schemas from '~/modules/examples/schemas'

import type { FastifyInstance } from 'fastify'
import type { Route, Schema } from '@/fastify/types'

export const register: Route.Register = (app, options, done) => {
  const exampleRouters = [
    registerOperations,
    registerBasicAuth,
    registerErrors,
    registerUsers,
  ]

  exampleRouters.forEach((route) => route(app))
  done()
}

export const options: Route.Options = {
  prefix: '/examples',
}

function registerOperations(app: FastifyInstance): SideEffect<FastifyInstance> {
  app.post<Schema.Domain<typeof schemas.operations.add>>(
    '/operations/add',
    { schema: schemas.operations.add },
    controllers.operations.add,
  )

  app.post<Schema.Domain<typeof schemas.operations.subtract>>(
    '/operations/subtract',
    { schema: schemas.operations.subtract },
    controllers.operations.subtract,
  )

  return app
}

function registerBasicAuth(app: FastifyInstance): SideEffect<FastifyInstance> {
  app.post<Schema.Domain<typeof schemas.basicAuth.create>>(
    '/basic-auth/create',
    { schema: schemas.basicAuth.create },
    controllers.basicAuth.create,
  )

  app.post(
    '/basic-auth/check',
    { preHandler: app.basicAuth },
    controllers.basicAuth.check,
  )

  return app
}

function registerErrors(app: FastifyInstance): SideEffect<FastifyInstance> {
  app.get(
    '/errors',
    controllers.errors.getAll,
  )

  app.get<Schema.Domain<typeof schemas.errors.throwError>>(
    '/errors/:error',
    { schema: schemas.errors.throwError },
    controllers.errors.throwError,
  )

  return app
}

function registerUsers(app: FastifyInstance): SideEffect<FastifyInstance> {
  app.get<Schema.Domain<typeof schemas.users.getNotes>>(
    '/users/:userID/notes',
    { schema: schemas.users.getNotes },
    controllers.users.getNotes,
  )

  app.post<Schema.Domain<typeof schemas.users.createNote>>(
    '/users/:userID/notes',
    { schema: schemas.users.createNote },
    controllers.users.createNote,
  )

  app.get<Schema.Domain<typeof schemas.users.getNote>>(
    '/users/:userID/notes/:noteID',
    { schema: schemas.users.getNote },
    controllers.users.getNote,
  )

  app.patch<Schema.Domain<typeof schemas.users.updateNote>>(
    '/users/:userID/notes/:noteID',
    { schema: schemas.users.updateNote },
    controllers.users.updateNote,
  )

  app.delete<Schema.Domain<typeof schemas.users.destroyNote>>(
    '/users/:userID/notes/:noteID',
    { schema: schemas.users.destroyNote },
    controllers.users.destroyNote,
  )

  return app
}
