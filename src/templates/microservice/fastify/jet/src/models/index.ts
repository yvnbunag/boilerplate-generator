import { Note } from '~/models/note'
import { model, connection } from '@/sequelize'

import type { Connection } from '@/sequelize/types'

/**
 * Define connections
 */
type ConnectionFactory = Connection.Factory<'test'>

export function create(connectionFactory: ConnectionFactory) {
  const connectionLocator = new connection.Locator(connectionFactory)
  const modelLocator = new model.Locator(
    connectionLocator,
    { Note: { initialize: Note.initialize, connection: 'test' } },
  )

  // #region Models association
  /**
   * Associate models here, e.g.:
   *
   * const User = modelLocator.get('User')
   * const Note = modelLocator.get('Note')
   *
   * User.hasMany(Note)
   * Note.belongsTo(User)
   */
  // #endregion

  return modelLocator
}

export async function sync(locator: model.Locator): AsyncSideEffect {
  const models = Object.values(locator.getAll())

  await Promise.all(models.map((model) => model.sync()))
}

export {
  Note,
}
