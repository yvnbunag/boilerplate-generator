import { Model as SequelizeModel, DataTypes } from 'sequelize'

import type { Optional, Sequelize } from 'sequelize'
import type { Model } from '@/sequelize/types'

export namespace Note {
  export interface Attributes {
    id: number,
    userID: number,
    title: string,
    note: string,
  }

  export type CreationAttributes = Optional<Attributes, 'id'>

  export type RawAttributes = CreationAttributes & Model.Attributes.Managed
}

export class Note extends SequelizeModel<
  Note.Attributes,
  Note.CreationAttributes
> implements Note.Attributes {
  public readonly id!: number
  public userID!: number
  public title!: string
  public note!: string

  public readonly createdAt!: Note.RawAttributes['createdAt']
  public readonly updatedAt!: Note.RawAttributes['updatedAt']
  public readonly deletedAt!: Note.RawAttributes['deletedAt']

  static table = 'notes'

  static initialize = function initializeNoteModel(connection: Sequelize) {
    return Note.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        userID: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        title: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        note: {
          type: DataTypes.TEXT,
          allowNull: false,
          defaultValue: '',
        },
      },
      {
        sequelize: connection,
        tableName: Note.table,
        paranoid: true,
      },
    )
  }
}

export namespace Note {
  export type Model = typeof Note
}
