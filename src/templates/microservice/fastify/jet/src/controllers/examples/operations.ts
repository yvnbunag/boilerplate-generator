import { operations as handlers } from '~/modules/examples/handlers'

import type { operations as schemas } from '~/modules/examples/schemas'
import type { Controller } from '@/fastify/types'

export const add: Controller<
  typeof schemas.add
> = function(request, reply) {
  const sum = handlers.add(...request.body.values)

  reply.sendStatusJSON(sum)
}

export const subtract: Controller<
  typeof schemas.subtract
> = function (request, reply) {
  const difference = handlers.subtract(...request.body.values)

  reply.sendStatusJSON(difference)
}
