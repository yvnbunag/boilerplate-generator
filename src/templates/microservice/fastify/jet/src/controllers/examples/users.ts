import { users as handlers } from '~/modules/examples/handlers'

import type { users as schemas } from '~/modules/examples/schemas'
import type { Controller } from '@/fastify/types'

export const getNotes: Controller<
  typeof schemas.getNotes
> = async function (request, reply) {
  const { userID } = request.params
  const { page, limit } = request.query
  const Note = this.dependencies.models.get('Note')
  const notes = await handlers.getNotes(Note, userID, { page, limit })

  reply.sendStatusJSON(notes)
}

export const createNote: Controller<
  typeof schemas.createNote
> = async function (request, reply) {
  const { userID } = request.params
  const attributes = { ...request.body, userID }
  const Note = this.dependencies.models.get('Note')
  const note = await handlers.createNote(Note, attributes)

  reply.sendStatusJSON(note)
}

export const getNote: Controller<
  typeof schemas.getNote
> = async function (request, reply) {
  const { userID, noteID } = request.params
  const Note = this.dependencies.models.get('Note')
  const note = await handlers.getNote(Note, userID, noteID)

  reply.sendStatusJSON(note)
}

export const updateNote: Controller<
  typeof schemas.updateNote
> = async function (request, reply) {
  const { userID, noteID } = request.params
  const { body } = request
  const Note = this.dependencies.models.get('Note')
  const note = await handlers.updateNote(Note, userID, noteID, body)

  reply.sendStatusJSON(note)
}

export const destroyNote: Controller<
  typeof schemas.destroyNote
> = async function (request, reply) {
  const { userID, noteID } = request.params
  const Note = this.dependencies.models.get('Note')

  await handlers.destroyNote(Note, userID, noteID)

  reply.sendStatusJSON()
}
