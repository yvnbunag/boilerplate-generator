import { define } from '@/fastify/schema'

export const add = define({
  body: {
    type: 'object',
    required: ['values'],
    properties: {
      values: {
        type: 'array',
        items: { type: 'number' },
      },
    },
  },
} as const)

export const subtract = define({
  body: {
    type: 'object',
    required: ['values'],
    properties: {
      values: {
        type: 'array',
        items: { type: 'number' },
      },
    },
  },
} as const)
