import { errorMap } from '~/modules/examples/error-map'
import { NotFoundError } from '@/fastify/errors'

export function getAll(): Array<keyof typeof errorMap> {
  return Object.keys(errorMap)
}

export function throwHTTPError(error: string, message?: string): never {
  if (error in errorMap) throw new errorMap[error](message)

  throw new NotFoundError(`${error} not found`)
}

export { errorMap }
