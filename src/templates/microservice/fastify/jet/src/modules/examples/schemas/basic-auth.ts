import { define } from '@/fastify/schema'

export const create = define({
  body: {
    type: 'object',
    required: ['username' , 'password'],
    properties: {
      username: { type: 'string' },
      password: { type: 'string' },
    },
  },
} as const)
