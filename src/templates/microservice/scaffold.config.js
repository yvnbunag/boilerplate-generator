/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'Microservice (Back End)',
  isDomain: true,
}

module.exports = config
