import { ExamplesModule } from '@/examples/examples.module';
import { DatabaseModule } from '@/global/database/database.module';
import { RequestModule } from '@/global/request/request.module';
import { ResponseModule } from '@/global/response/response.module';
import { Module } from '@nestjs/common';
import { LoggerModule } from './global/logger/logger.module';

@Module({
  imports: [
    RequestModule,
    ResponseModule,
    DatabaseModule,
    LoggerModule,
    ExamplesModule,
  ],
})
export class AppModule {}
