import { AuthModule } from '@/examples/auth/auth.module';
import { UsersModule } from '@/examples/users/users.module';
import { Module } from '@nestjs/common';
import { NotesModule } from './notes/notes.module';

@Module({
  imports: [AuthModule, UsersModule, NotesModule],
})
export class ExamplesModule {}
