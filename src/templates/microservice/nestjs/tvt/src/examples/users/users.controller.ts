import { AuthGuard } from '@/examples/auth/guards/auth.guard';
import { RolesGuard } from '@/examples/auth/guards/roles.guard';
import { UserRequestParams } from '@/examples/users/models/user-request-params';
import { UserResponseDto } from '@/examples/users/models/user-response-dto';
import { UsersResponseDto } from '@/examples/users/models/users-response-dto';
import { UsersService } from '@/examples/users/users.service';
import { ResponseDtoInterceptor } from '@/global/response/response-dto.interceptor';
import { PaginatedRequestParams } from '@/lib/pagination';
import {
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { PermissionGuard } from '../auth/guards/permission.guard';
import { ResourcesGuard } from '../auth/guards/resources.guard';

@Controller('/examples/users')
@UseGuards(AuthGuard, RolesGuard, PermissionGuard, ResourcesGuard)
@RolesGuard.require(['user', 'admin'])
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get('/')
  @UseInterceptors(ResponseDtoInterceptor.use(UsersResponseDto))
  @PermissionGuard.require('get:user')
  @RolesGuard.require(['admin'])
  getUsers(
    @Query() pagination: PaginatedRequestParams,
  ): Promise<UsersResponseDto> {
    return this.userService.getUsers(pagination);
  }

  @Get('/:userId')
  @UseInterceptors(ResponseDtoInterceptor.use(UserResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('get:user')
  async getUser(
    @Param() { userId }: UserRequestParams,
  ): Promise<UserResponseDto> {
    const user = await this.userService.getUser(userId);

    if (!user) {
      throw new NotFoundException('not found');
    }

    return user;
  }

  @Delete('/:userId')
  @UseInterceptors(ResponseDtoInterceptor.use(UserResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('delete:user')
  @RolesGuard.require(['admin'])
  async deleteUser(
    @Param() { userId }: UserRequestParams,
  ): Promise<UserResponseDto> {
    const user = await this.userService.softDeleteUser(userId);

    if (!user) {
      throw new NotFoundException('not found');
    }

    return user;
  }
}
