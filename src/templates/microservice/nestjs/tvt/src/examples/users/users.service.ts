import { EmailTakenError } from '@/examples/users/errors/email-taken-error';
import { RolesService } from '@/examples/users/roles.service';
import { ExampleUserPermission } from '@/global/database/entities/example-user-permission.entity';
import { ExampleUserRole } from '@/global/database/entities/example-user-role.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import {
  CURSOR_DEFAULT_SIZE,
  CursorPagination,
  CursorPaginationResult,
} from '@/lib/pagination';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoreThan, Repository } from 'typeorm';
import { PermissionsService } from './permissions.service';

@Injectable()
export class UsersService {
  static errors = {
    EmailTakenError,
  };

  constructor(
    @InjectRepository(ExampleUser)
    private usersRepository: Repository<ExampleUser>,
    private rolesService: RolesService,
    private permissionsService: PermissionsService,
  ) {}

  async createUser({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Promise<ExampleUser> {
    if (await this.usersRepository.findOneBy({ email })) {
      throw new UsersService.errors.EmailTakenError();
    }

    const user = this.usersRepository.create({ email }).setPassword(password);
    const roles = await this.rolesService.getRoles(['user']);
    const permissions = await this.permissionsService.getPermissions([
      'get:user',
      'get:note',
      'create:note',
      'update:note',
      'delete:note',
    ]);

    user.userRoles = roles.map((role) =>
      ExampleUserRole.create({ user, role }),
    );
    user.userPermissions = permissions.map((permission) =>
      ExampleUserPermission.create({ user, permission }),
    );

    return this.usersRepository.save(user);
  }

  async getUsers({
    cursor = null,
    size = CURSOR_DEFAULT_SIZE,
  }: CursorPagination = {}): Promise<CursorPaginationResult<ExampleUser>> {
    const users = await this.usersRepository.find({
      where: {
        ...(cursor !== null ? { id: MoreThan(Number(cursor)) } : {}),
      },
      take: size + 1,
    });
    const results = users.slice(0, size);
    const nextCursor = users[size]
      ? String(results[results.length - 1].id)
      : null;

    return {
      results,
      previousCursor: cursor,
      nextCursor,
    };
  }

  getUser(id: number): Promise<ExampleUser | null> {
    return this.usersRepository.findOneBy({ id });
  }

  async getUserByCredentials({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Promise<ExampleUser | null> {
    const user = await this.usersRepository.findOne({
      where: { email },
      relations: [
        'userRoles',
        'userRoles.role',
        'userPermissions',
        'userPermissions.permission',
      ],
    });

    if (user?.comparePassword(password)) {
      return user;
    }

    return null;
  }

  async softDeleteUser(id: number): Promise<ExampleUser | null> {
    const user = await this.getUser(id);

    if (!user) {
      return null;
    }

    await this.usersRepository.softDelete(user.id);

    return user;
  }
}
