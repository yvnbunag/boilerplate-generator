import { ExampleRole } from '@/global/database/entities/example-role.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(ExampleRole)
    private rolesRepository: Repository<ExampleRole>,
  ) {}

  getRoles(roles: ExampleRole['role'][]) {
    return this.rolesRepository.find({ where: { role: In(roles) } });
  }
}
