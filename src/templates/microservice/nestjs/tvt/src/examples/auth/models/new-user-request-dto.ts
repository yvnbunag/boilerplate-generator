import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class NewUserRequestDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
