import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UserTokenResponseDto {
  @Expose()
  token = '';
}
