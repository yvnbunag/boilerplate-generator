import { RolesGuard } from '@/examples/auth/guards/roles.guard';
import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { createTestJwtUser } from 'test/lib/create-test-jwt-user';
import { RequestInjector } from 'test/lib/request-injector';
import { describe, expect, it } from 'vitest';

describe('RolesGuard', () => {
  it('should return successful response on unguarded controller and handler', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unauthenticated request', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    class TestController {
      @Get()
      @RolesGuard.require(['user', 'admin'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return successful response on matching handler role', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    @RequestInjector.inject('user', createTestJwtUser({ roles: ['user'] }))
    class TestController {
      @Get()
      @RolesGuard.require(['user', 'admin'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return successful response on matching controller role', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    @RolesGuard.require(['user', 'admin'])
    @RequestInjector.inject('user', createTestJwtUser({ roles: ['admin'] }))
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unmatched handler role', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    @RequestInjector.inject('user', createTestJwtUser({ roles: ['user'] }))
    class TestController {
      @Get()
      @RolesGuard.require(['admin'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return unauthorized response on unmatched controller role', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    @RolesGuard.require(['user'])
    @RequestInjector.inject('user', createTestJwtUser({ roles: ['admin'] }))
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should prioritize handler roles over controller roles', async () => {
    @Controller()
    @UseGuards(RolesGuard)
    @RolesGuard.require(['user'])
    @RequestInjector.inject('user', createTestJwtUser({ roles: ['user'] }))
    class TestController {
      @Get()
      @RolesGuard.require(['admin'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });
});
