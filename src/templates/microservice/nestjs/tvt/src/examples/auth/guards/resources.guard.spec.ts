import {
  Resource,
  ResourcesGuard,
} from '@/examples/auth/guards/resources.guard';
import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { createTestJwtUser } from 'test/lib/create-test-jwt-user';
import { RequestInjector } from 'test/lib/request-injector';
import { describe, expect, it } from 'vitest';

describe('ResourcesGuard', () => {
  it('should return successful response on unguarded controller and handler', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/1000');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unauthenticated request', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    class TestController {
      @Get()
      @ResourcesGuard.require(['userId'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/1000');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return successful response on matching handler resources', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @RequestInjector.inject('user', createTestJwtUser({ userId: 1000 }))
    class TestController {
      @Get()
      @ResourcesGuard.require(['userId'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/1000');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return successful response on matching controller resources', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['userId'])
    @RequestInjector.inject('user', createTestJwtUser({ userId: 1000 }))
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/1000');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unmatched handler resources', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @RequestInjector.inject('user', createTestJwtUser({ userId: 1000 }))
    class TestController {
      @Get()
      @ResourcesGuard.require(['userId'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/2000');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return unauthorized response on unmatched controller resources', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['userId'])
    @RequestInjector.inject('user', createTestJwtUser({ userId: 2000 }))
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/1000');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should prioritize handler resources over controller resources', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['noteId' as Resource])
    @RequestInjector.inject('user', createTestJwtUser({ userId: 1000 }))
    class TestController {
      @Get()
      @ResourcesGuard.require(['userId'])
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/2000');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return successful response if user is admin', async () => {
    @Controller('/users/:userId')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['userId'])
    @RequestInjector.inject(
      'user',
      createTestJwtUser({ userId: 1000, roles: ['admin'] }),
    )
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/users/2000');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return require all resources to be allowed', async () => {
    @Controller('/users/:userId/notes/:noteId')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['userId', 'noteId' as Resource])
    @RequestInjector.inject(
      'user',
      // @ts-expect-error Simulate another required resource
      createTestJwtUser({ userId: 1000, noteId: 3000 }),
    )
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get(
      '/users/1000/notes/2000',
    );

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return internal server error if resource is not in path parameters', async () => {
    @Controller('/')
    @UseGuards(ResourcesGuard)
    @ResourcesGuard.require(['userId'])
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const { statusCode, body } = await request(nestApp.getHttpServer()).get(
      '/',
    );

    expect(statusCode).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    expect(body.message).toBe('required resources are not in path parameters');
  });
});
