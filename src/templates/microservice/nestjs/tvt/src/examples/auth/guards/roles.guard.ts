import { JwtUser, Role } from '@/examples/auth/auth.types';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  SetMetadata,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  private static requiredRolesKey = Symbol('roles-guard-required-roles-key');

  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const requiredRoles = RolesGuard.getRoles(this.reflector, context);

    if (!requiredRoles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user: JwtUser = request.user;

    if (!user || !user.roles.some((role) => requiredRoles.includes(role))) {
      throw new UnauthorizedException('unauthorized');
    }

    return true;
  }

  static require(roles: [Role, ...Role[]]) {
    return SetMetadata(RolesGuard.requiredRolesKey, roles);
  }

  private static getRoles(reflector: Reflector, context: ExecutionContext) {
    return reflector.getAllAndOverride<Role[]>(RolesGuard.requiredRolesKey, [
      context.getHandler(),
      context.getClass(),
    ]);
  }
}
