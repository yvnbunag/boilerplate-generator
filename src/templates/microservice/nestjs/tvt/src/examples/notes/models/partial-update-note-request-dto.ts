import { UpdateDto } from '@/lib/models/update-dto';
import { IsOptional, IsString, MaxLength } from 'class-validator';

export class PartialUpdateNoteRequestDto extends UpdateDto {
  @IsOptional()
  @MaxLength(50)
  @IsString()
  title?: string;

  @IsOptional()
  @MaxLength(500)
  @IsString()
  contents?: string;
}
