import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class CreateNoteRequestParams {
  @IsNumber()
  @Type(() => Number)
  userId: number;
}
