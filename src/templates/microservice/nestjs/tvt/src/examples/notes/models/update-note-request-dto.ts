import { UpdateDto } from '@/lib/models/update-dto';
import { IsString, MaxLength } from 'class-validator';

export class UpdateNoteRequestDto extends UpdateDto {
  @MaxLength(50)
  @IsString()
  title: string;

  @MaxLength(500)
  @IsString()
  contents: string;
}
