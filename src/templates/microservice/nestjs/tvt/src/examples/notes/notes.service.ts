import { ExampleNote } from '@/global/database/entities/example-note.entity';
import {
  CURSOR_DEFAULT_SIZE,
  CursorPagination,
  CursorPaginationResult,
} from '@/lib/pagination';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoreThan, Repository } from 'typeorm';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(ExampleNote)
    private notesRepository: Repository<ExampleNote>,
  ) {}

  createNote(
    newNote: Omit<ExampleNote, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>,
  ): Promise<ExampleNote> {
    const note = this.notesRepository.create(newNote);

    return this.notesRepository.save(note);
  }

  async getNotes(
    userId: number,
    { cursor = null, size = CURSOR_DEFAULT_SIZE }: CursorPagination = {},
  ): Promise<CursorPaginationResult<ExampleNote>> {
    const notes = await this.notesRepository.find({
      where: {
        userId,
        ...(cursor !== null ? { id: MoreThan(Number(cursor)) } : {}),
      },
      take: size + 1,
    });
    const results = notes.slice(0, size);
    const nextCursor = notes[size]
      ? String(results[results.length - 1].id)
      : null;

    return {
      results,
      previousCursor: cursor,
      nextCursor,
    };
  }

  getNote(userId: number, id: number): Promise<ExampleNote | null> {
    return this.notesRepository.findOneBy({ userId, id });
  }

  async updateNote(note: ExampleNote): Promise<ExampleNote> {
    await this.notesRepository.update(note.id, note);

    return note;
  }

  async softDeleteNote(
    userId: number,
    id: number,
  ): Promise<ExampleNote | null> {
    const note = await this.getNote(userId, id);

    if (!note) {
      return null;
    }

    await this.notesRepository.softDelete(note.id);

    return note;
  }
}
