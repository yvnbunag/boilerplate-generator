import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class NoteRequestParams {
  @IsNumber()
  @Type(() => Number)
  userId: number;

  @IsNumber()
  @Type(() => Number)
  noteId: number;
}
