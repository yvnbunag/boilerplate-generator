import { BadRequestException } from '@nestjs/common';
import {
  ClassConstructor,
  Exclude,
  Expose,
  Transform,
  TransformationType,
  Type,
} from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, Max, Min } from 'class-validator';

export type CursorPagination = {
  cursor?: string | null;
  size?: number;
};

export type CursorPaginationResult<Result> = {
  results: Result[];
  previousCursor: string | null;
  nextCursor: string | null;
};

export const CURSOR_DEFAULT_SIZE = 100;

export class PaginatedRequestParams {
  @IsOptional()
  @IsNotEmpty()
  @Transform(({ value, key }) => decodeCursor(value, key))
  cursor?: string | null = null;

  @IsOptional()
  @Max(500)
  @Min(1)
  @IsNumber()
  @Type(() => Number)
  size?: number;
}

export function PaginatedResponseDto<Dto>(
  DtoClass: ClassConstructor<Dto>,
): ClassConstructor<CursorPaginationResult<Dto>> {
  @Exclude()
  class PaginatedDto {
    @Expose()
    @Type(() => DtoClass)
    results: Dto[] = [];

    @Expose()
    @TransformToCursor()
    previousCursor: string | null = null;

    @Expose()
    @TransformToCursor()
    nextCursor: string | null = null;
  }

  return PaginatedDto;
}

function encodeCursor(cursor: string | null): string {
  if (typeof cursor !== 'string') {
    return null;
  }

  return Buffer.from(cursor, 'utf-8').toString('base64');
}

function decodeCursor(cursor: string | null, key = 'cursor') {
  if (typeof cursor !== 'string') {
    return null;
  }

  const decodedCursor = Buffer.from(cursor, 'base64').toString('utf-8');

  if (cursor !== encodeCursor(decodedCursor)) {
    throw new BadRequestException(`${key} must be a valid cursor`);
  }

  return decodedCursor;
}

function TransformToCursor() {
  return Transform(({ type, value }) => {
    if (type === TransformationType.PLAIN_TO_CLASS) {
      return encodeCursor(value);
    }

    return value;
  });
}
