import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
class RequireOneFieldPipe implements PipeTransform {
  transform(value: unknown) {
    if (value instanceof UpdateDto) {
      if (value.definedFields().length <= 0) {
        throw new BadRequestException('at least one field required');
      }
    }

    return value;
  }
}

export class UpdateDto {
  static RequireOneFieldPipe = RequireOneFieldPipe;

  fields() {
    return Object.keys(this);
  }

  definedFields() {
    return this.fields().filter((field) => this[field] !== undefined);
  }

  assign<Target>(target: Target): Target {
    this.definedFields().forEach((field) => {
      target[field] = this[field];
    });

    return target;
  }
}
