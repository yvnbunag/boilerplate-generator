import { PaginatedRequestParams, PaginatedResponseDto } from '@/lib/pagination';
import { BadRequestException } from '@nestjs/common';
import { ClassTransformOptions, plainToClass } from 'class-transformer';
import { ValidatorOptions, validate } from 'class-validator';
import { describe, expect, it } from 'vitest';

const transformOptions: ClassTransformOptions = {
  exposeDefaultValues: true,
};
const validateOptions: ValidatorOptions = {
  stopAtFirstError: true,
};

describe('PaginatedResponseDto', () => {
  it('should have default values', () => {
    const response = plainToClass(
      PaginatedResponseDto(String),
      {},
      transformOptions,
    );

    expect(response).toEqual({
      previousCursor: null,
      nextCursor: null,
      results: [],
    });
  });

  it('should transform results', () => {
    const response = plainToClass(
      PaginatedResponseDto(String),
      { results: [1, 2] },
      transformOptions,
    );

    expect(response.results).toEqual(['1', '2']);
  });

  it('should encode cursors', () => {
    const response = plainToClass(
      PaginatedResponseDto(String),
      {
        previousCursor: 'previous-cursor',
        nextCursor: 'next-cursor',
      },
      transformOptions,
    );

    expect(response.previousCursor).toEqual('cHJldmlvdXMtY3Vyc29y');
    expect(response.nextCursor).toEqual('bmV4dC1jdXJzb3I=');
  });

  it('should return null for non string cursors', () => {
    const response = plainToClass(
      PaginatedResponseDto(String),
      {
        previousCursor: 1,
        nextCursor: null,
      },
      transformOptions,
    );

    expect(response.previousCursor).toEqual(null);
    expect(response.nextCursor).toEqual(null);
  });
});

describe('PaginatedRequestParams', () => {
  it('should not require any fields', async () => {
    const pagination = plainToClass(
      PaginatedRequestParams,
      {},
      transformOptions,
    );
    const errors = await validate(pagination, validateOptions);

    expect(pagination).toEqual({
      cursor: null,
    });
    expect(errors).toEqual([]);
  });

  it('should extract and decode cursor', async () => {
    const { nextCursor } = plainToClass(
      PaginatedResponseDto(String),
      { nextCursor: 'next-cursor' },
      transformOptions,
    );
    const pagination = plainToClass(
      PaginatedRequestParams,
      { cursor: nextCursor },
      transformOptions,
    );
    const errors = await validate(pagination, validateOptions);

    expect(pagination.cursor).toEqual('next-cursor');
    expect(errors).toEqual([]);
  });

  it('should require cursor to not be empty', async () => {
    const errors = await validate(
      plainToClass(PaginatedRequestParams, { cursor: '' }, transformOptions),
      validateOptions,
    );

    expect(errors).toHaveLength(1);
    expect(errors[0].constraints).toEqual({
      isNotEmpty: 'cursor should not be empty',
    });
  });

  it('should require cursor to be valid', async () => {
    const transform = () =>
      plainToClass(
        PaginatedRequestParams,
        { cursor: 'invalid-cursor' },
        transformOptions,
      );

    expect(transform).toThrow(BadRequestException);
    expect(transform).toThrow('cursor must be a valid cursor');
  });

  it('should extract size', async () => {
    const pagination = plainToClass(
      PaginatedRequestParams,
      { size: '1' },
      transformOptions,
    );
    const errors = await validate(pagination);

    expect(pagination.size).toEqual(1);
    expect(errors).toEqual([]);
  });

  it('should require size to be a number', async () => {
    const errors = await validate(
      plainToClass(
        PaginatedRequestParams,
        { size: 'string' },
        transformOptions,
      ),
      validateOptions,
    );

    expect(errors).toHaveLength(1);
    expect(errors[0].constraints).toEqual({
      isNumber: 'size must be a number conforming to the specified constraints',
    });
  });

  it('should require size to be a minimum of 1', async () => {
    const errors = await validate(
      plainToClass(PaginatedRequestParams, { size: '0' }, transformOptions),
      validateOptions,
    );

    expect(errors).toHaveLength(1);
    expect(errors[0].constraints).toEqual({
      min: 'size must not be less than 1',
    });
  });

  it('should require size to be a maximum of 500', async () => {
    const errors = await validate(
      plainToClass(PaginatedRequestParams, { size: '501' }, transformOptions),
      validateOptions,
    );

    expect(errors).toHaveLength(1);
    expect(errors[0].constraints).toEqual({
      max: 'size must not be greater than 500',
    });
  });
});
