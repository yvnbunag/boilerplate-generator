import { ConfigModule } from '@/global/config/config.module';
import { ConfigService } from '@/global/config/config.service';
import dataSource from '@/global/database/database.datasource';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSourceOptions } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService): DataSourceOptions => {
        if (configService.environment === ConfigService.Environment.TEST) {
          return {
            type: 'sqlite',
            database: ':memory:',
            entities: dataSource.options.entities,
            synchronize: true,
          };
        }

        return dataSource.options;
      },
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
