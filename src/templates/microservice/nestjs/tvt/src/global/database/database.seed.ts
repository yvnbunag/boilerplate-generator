import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { ExampleRole } from '@/global/database/entities/example-role.entity';
import { ExampleUserPermission } from '@/global/database/entities/example-user-permission.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import { DataSource, In, QueryRunner } from 'typeorm';
import { ExampleUserRole } from './entities/example-user-role.entity';

export async function seed(dataSource: DataSource) {
  const queryRunner = dataSource.createQueryRunner();
  await queryRunner.connect();
  await queryRunner.startTransaction();

  try {
    await seedRoles(queryRunner);
    await seedPermissions(queryRunner);
    await seedAdminUser(queryRunner);

    await queryRunner.commitTransaction();
  } catch (error) {
    await queryRunner.rollbackTransaction();

    throw error;
  } finally {
    queryRunner.release();
  }
}

function seedRoles(queryRunner: QueryRunner) {
  return queryRunner.manager
    .getRepository(ExampleRole)
    .save([
      ExampleRole.create({ role: 'user' }),
      ExampleRole.create({ role: 'admin' }),
    ]);
}

function seedPermissions(queryRunner: QueryRunner) {
  return queryRunner.manager
    .getRepository(ExamplePermission)
    .save([
      ExamplePermission.create({ permission: 'get:user' }),
      ExamplePermission.create({ permission: 'create:user' }),
      ExamplePermission.create({ permission: 'update:user' }),
      ExamplePermission.create({ permission: 'delete:user' }),
      ExamplePermission.create({ permission: 'get:note' }),
      ExamplePermission.create({ permission: 'create:note' }),
      ExamplePermission.create({ permission: 'update:note' }),
      ExamplePermission.create({ permission: 'delete:note' }),
    ]);
}

async function seedAdminUser(queryRunner: QueryRunner) {
  const email = process.env.ADMIN_USERNAME || '';
  const password = process.env.ADMIN_PASSWORD || '';
  const adminRoles = process.env.ADMIN_ROLES?.split(',') || [];
  const adminPermissions = process.env.ADMIN_PERMISSIONS?.split(',') || [];

  if (!email && !password) {
    return;
  }

  if (!email) {
    throw new Error('seed for admin user must have an email');
  }

  if (!password) {
    throw new Error('seed for admin user must have a password');
  }

  if (!adminRoles.length) {
    throw new Error('seed for admin user must have at least one role');
  }

  if (!adminPermissions.length) {
    throw new Error('seed for admin user must have at least one permission');
  }

  const roles = await queryRunner.manager
    .getRepository(ExampleRole)
    .find({ where: { role: In(adminRoles) } });
  const permissions = await queryRunner.manager
    .getRepository(ExamplePermission)
    .find({ where: { permission: In(adminPermissions) } });
  const user = ExampleUser.create({ email, password });

  user.userRoles = roles.map((role) => ExampleUserRole.create({ user, role }));
  user.userPermissions = permissions.map((permission) =>
    ExampleUserPermission.create({ user, permission }),
  );

  await queryRunner.manager.getRepository(ExampleUser).save(user);
}
