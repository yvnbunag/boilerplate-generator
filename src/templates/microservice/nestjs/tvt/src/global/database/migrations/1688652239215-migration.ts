import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1688652239215 implements MigrationInterface {
    name = 'Migration1688652239215'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`example_user_permission\` (\`id\` int NOT NULL AUTO_INCREMENT, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, \`userId\` int NULL, \`permissionId\` int NULL, INDEX \`IDX_example_user_permission_user\` (\`userId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`example_permission\` (\`id\` int NOT NULL AUTO_INCREMENT, \`permission\` varchar(15) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, INDEX \`IDX_example_permission_permission\` (\`permission\`), UNIQUE INDEX \`IDX_095e0f34ece81e5946c7f68cad\` (\`permission\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`example_user_permission\` ADD CONSTRAINT \`FK_62ec6c4369ed48a0872632b51c4\` FOREIGN KEY (\`userId\`) REFERENCES \`example_user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`example_user_permission\` ADD CONSTRAINT \`FK_aa9df2303c9ade6f1f1c9a4e08d\` FOREIGN KEY (\`permissionId\`) REFERENCES \`example_permission\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`example_user_permission\` DROP FOREIGN KEY \`FK_aa9df2303c9ade6f1f1c9a4e08d\``);
        await queryRunner.query(`ALTER TABLE \`example_user_permission\` DROP FOREIGN KEY \`FK_62ec6c4369ed48a0872632b51c4\``);
        await queryRunner.query(`DROP INDEX \`IDX_095e0f34ece81e5946c7f68cad\` ON \`example_permission\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_permission_permission\` ON \`example_permission\``);
        await queryRunner.query(`DROP TABLE \`example_permission\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_user_permission_user\` ON \`example_user_permission\``);
        await queryRunner.query(`DROP TABLE \`example_user_permission\``);
    }

}
