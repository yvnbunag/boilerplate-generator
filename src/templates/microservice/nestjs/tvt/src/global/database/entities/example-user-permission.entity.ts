import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import {
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class ExampleUserPermission {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ExampleUser, (user) => user.userPermissions)
  @Index('IDX_example_user_permission_user')
  user: ExampleUser;

  @ManyToOne(
    () => ExamplePermission,
    (permission) => permission.userPermissions,
  )
  permission: ExamplePermission;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  static create({
    user,
    permission,
  }: Pick<
    ExampleUserPermission,
    'user' | 'permission'
  >): ExampleUserPermission {
    const exampleUserPermission = new ExampleUserPermission();

    exampleUserPermission.user = user;
    exampleUserPermission.permission = permission;

    return exampleUserPermission;
  }
}
