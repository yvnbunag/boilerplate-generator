import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1688645905079 implements MigrationInterface {
    name = 'Migration1688645905079'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`example_user_role\` (\`id\` int NOT NULL AUTO_INCREMENT, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, \`userId\` int NULL, \`roleId\` int NULL, INDEX \`IDX_example_user_role_user\` (\`userId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`example_role\` (\`id\` int NOT NULL AUTO_INCREMENT, \`role\` varchar(15) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, INDEX \`IDX_example_role_role\` (\`role\`), UNIQUE INDEX \`IDX_835702055735aea5c2ffa1a715\` (\`role\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`example_user_role\` ADD CONSTRAINT \`FK_cd1d93e8738020276a5f1c7c70e\` FOREIGN KEY (\`userId\`) REFERENCES \`example_user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`example_user_role\` ADD CONSTRAINT \`FK_6bbb39bc2c36aef2b67b480d1cb\` FOREIGN KEY (\`roleId\`) REFERENCES \`example_role\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`example_user_role\` DROP FOREIGN KEY \`FK_6bbb39bc2c36aef2b67b480d1cb\``);
        await queryRunner.query(`ALTER TABLE \`example_user_role\` DROP FOREIGN KEY \`FK_cd1d93e8738020276a5f1c7c70e\``);
        await queryRunner.query(`DROP INDEX \`IDX_835702055735aea5c2ffa1a715\` ON \`example_role\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_role_role\` ON \`example_role\``);
        await queryRunner.query(`DROP TABLE \`example_role\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_user_role_user\` ON \`example_user_role\``);
        await queryRunner.query(`DROP TABLE \`example_user_role\``);
    }

}
