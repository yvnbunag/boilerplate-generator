import { ExampleRole } from '@/global/database/entities/example-role.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import {
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class ExampleUserRole {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ExampleUser, (user) => user.userRoles)
  @Index('IDX_example_user_role_user')
  user: ExampleUser;

  @ManyToOne(() => ExampleRole, (role) => role.userRoles)
  role: ExampleRole;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  static create({
    user,
    role,
  }: Pick<ExampleUserRole, 'user' | 'role'>): ExampleUserRole {
    const exampleUserRole = new ExampleUserRole();

    exampleUserRole.user = user;
    exampleUserRole.role = role;

    return exampleUserRole;
  }
}
