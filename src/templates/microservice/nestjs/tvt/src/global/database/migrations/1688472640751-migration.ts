import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1688472640751 implements MigrationInterface {
    name = 'Migration1688472640751'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX \`IDX_example_note_userId\` ON \`example_note\` (\`userId\`)`);
        await queryRunner.query(`CREATE INDEX \`IDX_example_note_userId_title\` ON \`example_note\` (\`userId\`, \`title\`)`);
        await queryRunner.query(`CREATE INDEX \`IDX_example_user_email\` ON \`example_user\` (\`email\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_example_user_email\` ON \`example_user\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_note_userId_title\` ON \`example_note\``);
        await queryRunner.query(`DROP INDEX \`IDX_example_note_userId\` ON \`example_note\``);
    }

}
