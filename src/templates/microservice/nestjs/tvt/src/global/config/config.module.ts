import {
  ConfigService,
  EnvironmentVariables,
} from '@/global/config/config.service';
import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';

const envFilePath: string =
  process.env.NODE_ENV === 'test'
    ? `${process.cwd()}/.env.test`
    : `${process.cwd()}/.env`;

@Module({
  imports: [
    NestConfigModule.forRoot({
      envFilePath,
      validate: EnvironmentVariables.fromConfig,
      cache: true,
    }),
  ],
  providers: [ConfigService],
  exports: [ConfigService],
})
export class ConfigModule {}
