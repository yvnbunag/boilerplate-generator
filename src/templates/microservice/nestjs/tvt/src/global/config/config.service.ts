import { Injectable } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';
import { Type, plainToInstance } from 'class-transformer';
import { IsEnum, IsNumber, IsString, validateSync } from 'class-validator';

enum Environment {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  TEST = 'test',
}

enum LoggerLevel {
  FATAL = 'fatal',
  ERROR = 'error',
  WARN = 'warn',
  INFO = 'info',
  DEBUG = 'debug',
  TRACE = 'trace',
  SILENT = 'silent',
}

export class EnvironmentVariables {
  @IsEnum(Environment)
  NODE_ENV = Environment.PRODUCTION;

  @IsNumber()
  @Type(() => Number)
  PORT = 3001;

  @IsEnum(LoggerLevel)
  LOGGER_LEVEL = LoggerLevel.ERROR;

  @IsString()
  JWT_SECRET: string;

  @IsNumber()
  @Type(() => Number)
  JWT_EXPIRY = 60;

  static fromConfig(config: Record<string, unknown>) {
    const validatedConfig = plainToInstance(EnvironmentVariables, config);
    const errors = validateSync(validatedConfig, {
      skipMissingProperties: false,
    });

    if (errors.length > 0) {
      throw new Error(errors.toString());
    }

    return validatedConfig;
  }
}

@Injectable()
export class ConfigService {
  static Environment = Environment;

  constructor(
    private configService: NestConfigService<EnvironmentVariables, true>,
  ) {}

  get environment() {
    return this.get('NODE_ENV');
  }

  get port() {
    return this.get('PORT');
  }

  get logger() {
    return {
      level: this.get('LOGGER_LEVEL'),
      enabled: this.environment !== ConfigService.Environment.TEST,
    };
  }

  get jwt() {
    return {
      secret: this.get('JWT_SECRET'),
      expiry: this.get('JWT_EXPIRY'),
    };
  }

  private get<Key extends keyof EnvironmentVariables>(
    key: Key,
  ): EnvironmentVariables[Key] {
    return this.configService.get(key);
  }
}
