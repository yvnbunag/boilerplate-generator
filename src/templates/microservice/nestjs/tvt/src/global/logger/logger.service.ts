import { Injectable } from '@nestjs/common';
import { Logger as PinoLogger } from 'nestjs-pino';

@Injectable()
export class LoggerService extends PinoLogger {}
