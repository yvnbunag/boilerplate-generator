import { FailureFilter } from '@/global/response/failure.filter';
import { SuccessInterceptor } from '@/global/response/success.interceptor';
import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';

@Module({
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: SuccessInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: FailureFilter,
    },
  ],
})
export class ResponseModule {}
