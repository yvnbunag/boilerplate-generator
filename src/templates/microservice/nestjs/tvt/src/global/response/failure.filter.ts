import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

type FailureResponse = {
  success: false;
  error: string;
};

@Catch()
export class FailureFilter implements ExceptionFilter {
  private static defaultErrorMessage = 'internal server error';

  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost) {
    const { httpAdapter } = this.httpAdapterHost;
    const ctx = host.switchToHttp();
    const statusCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const response: FailureResponse = {
      success: false,
      error: FailureFilter.extractError(exception),
    };

    httpAdapter.reply(ctx.getResponse(), response, statusCode);
  }

  private static extractError(exception: unknown): string {
    if (exception instanceof BadRequestException) {
      const response = exception.getResponse();

      if (
        typeof response === 'object' &&
        'message' in response &&
        Array.isArray(response.message)
      ) {
        return response.message[0];
      }
    }

    if (exception instanceof Error) {
      return exception.message || FailureFilter.defaultErrorMessage;
    }

    return FailureFilter.defaultErrorMessage;
  }
}
