import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, map } from 'rxjs';

type SuccessfulResponse = {
  success: true;
  data?: unknown;
};

@Injectable()
export class SuccessInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(
        (data): SuccessfulResponse => ({
          success: true,
          ...(data !== undefined ? { data } : {}),
        }),
      ),
    );
  }
}
