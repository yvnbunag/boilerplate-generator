import { AppModule } from '@/app.module';
import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { createTestNote } from 'test/lib/create-test-note';
import { createTestUserLogin } from 'test/lib/create-test-user-login';
import { TestService } from 'test/lib/test-service';
import { beforeEach, describe, expect, it } from 'vitest';

let app: INestApplication;

beforeEach(async () => {
  const { nestApp, testModule } = await createTestApp({
    imports: [AppModule],
    providers: [TestService],
  });

  await testModule.get<TestService>(TestService).seedDatabase();

  app = nestApp;
});

describe('POST /examples/users/:userId/notes', () => {
  const title = 'Title';
  const contents = 'Contents';

  it('should create note', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title,
        contents,
      });

    expect(response.body).toEqual({
      success: true,
      data: {
        id: expect.any(Number),
        userId: user.id,
        title,
        contents,
      },
    });
    expect(response.statusCode).toBe(HttpStatus.CREATED);
  });

  it('should create note with extra fields', async () => {
    const id = 1000000;
    const userId = 1000000;
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        id,
        userId,
        title,
        contents,
      });

    expect(response.body).toHaveProperty('success', true);
    expect(response.body.data.id).not.toEqual(id);
    expect(response.body.data.userId).not.toEqual(userId);
    expect(response.statusCode).toBe(HttpStatus.CREATED);
  });

  it('should return bad request response if title is not provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        contents,
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if title is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: [],
        contents,
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if title is longer than 50 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: '-'.repeat(51),
        contents,
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be shorter than or equal to 50 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is not provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title,
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title,
        contents: [],
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is longer than 500 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title,
        contents: '-'.repeat(501),
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be shorter than or equal to 500 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no field is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({});

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no payload is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .post(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });
});

describe('GET /examples/users/:userId/notes', () => {
  it('should get empty notes', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: null,
        results: [],
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should get all notes', async () => {
    const user = await createTestUserLogin(app);

    await createTestNote(app, user, {
      title: 'First title',
      contents: 'First contents',
    });
    await createTestNote(app, user, {
      title: 'Second title',
      contents: 'Second contents',
    });

    const response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(response.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: null,
        results: [
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'First title',
            contents: 'First contents',
          },
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'Second title',
            contents: 'Second contents',
          },
        ],
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should be paginateable', async () => {
    const user = await createTestUserLogin(app);

    await createTestNote(app, user, {
      title: 'First title',
      contents: 'First contents',
    });
    await createTestNote(app, user, {
      title: 'Second title',
      contents: 'Second contents',
    });
    await createTestNote(app, user, {
      title: 'Third title',
      contents: 'Third contents',
    });

    const page1Response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes?size=1`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(page1Response.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: expect.any(String),
        results: [
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'First title',
            contents: 'First contents',
          },
        ],
      },
    });
    expect(page1Response.statusCode).toBe(HttpStatus.OK);

    const page2Response = await request(app.getHttpServer())
      .get(
        `/examples/users/${user.id}/notes?size=1&cursor=${page1Response.body.data.nextCursor}`,
      )
      .set('Authorization', 'Bearer ' + user.token);
    expect(page2Response.body).toEqual({
      success: true,
      data: {
        previousCursor: page1Response.body.data.nextCursor,
        nextCursor: expect.any(String),
        results: [
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'Second title',
            contents: 'Second contents',
          },
        ],
      },
    });
    expect(page2Response.statusCode).toBe(HttpStatus.OK);

    const page3Response = await request(app.getHttpServer())
      .get(
        `/examples/users/${user.id}/notes?size=1&cursor=${page2Response.body.data.nextCursor}`,
      )
      .set('Authorization', 'Bearer ' + user.token);
    expect(page3Response.body).toEqual({
      success: true,
      data: {
        previousCursor: page2Response.body.data.nextCursor,
        nextCursor: null,
        results: [
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'Third title',
            contents: 'Third contents',
          },
        ],
      },
    });
    expect(page3Response.statusCode).toBe(HttpStatus.OK);
  });

  it('should exclude deleted notes', async () => {
    const user = await createTestUserLogin(app);

    const firstNote = await createTestNote(app, user, {
      title: 'First title',
      contents: 'First contents',
    });
    await createTestNote(app, user, {
      title: 'Second title',
      contents: 'Second contents',
    });

    const deleteResponse = await request(app.getHttpServer())
      .delete(`/examples/users/${user.id}/notes/${firstNote.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(deleteResponse.statusCode).toBe(HttpStatus.OK);

    const getAllResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(getAllResponse.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: null,
        results: [
          {
            id: expect.any(Number),
            userId: user.id,
            title: 'Second title',
            contents: 'Second contents',
          },
        ],
      },
    });
    expect(getAllResponse.statusCode).toBe(HttpStatus.OK);
  });
});

describe('GET /examples/users/:userId/notes/:noteId', () => {
  it('should get note', async () => {
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user, {
      title: 'First title',
      contents: 'First contents',
    });

    const response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(response.body).toEqual({
      success: true,
      data: {
        id: note.id,
        userId: user.id,
        title: 'First title',
        contents: 'First contents',
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should return not found response if note does not exist', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });
});

describe('PUT /examples/users/:userId/notes/:noteId', () => {
  it('should update note', async () => {
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user, {
      title: 'Initial title',
      contents: 'Initial contents',
    });

    const updateResponse = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
        contents: 'Updated contents',
      });
    expect(updateResponse.body).toEqual({
      success: true,
      data: {
        id: note.id,
        userId: user.id,
        title: 'Updated title',
        contents: 'Updated contents',
      },
    });
    expect(updateResponse.statusCode).toBe(HttpStatus.OK);

    const getResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(getResponse.body).toHaveProperty('data.title', 'Updated title');
    expect(getResponse.body).toHaveProperty(
      'data.contents',
      'Updated contents',
    );
  });

  it('should update note with extra fields', async () => {
    const id = 1000000;
    const userId = 1000000;
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        id,
        userId,
        title: 'Updated title',
        contents: 'Updated contents',
      });

    expect(response.body).toHaveProperty('success', true);
    expect(response.body.data.id).not.toEqual(id);
    expect(response.body.data.userId).not.toEqual(userId);
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should return not found response if note does not exist', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
        contents: 'Updated contents',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });

  it('should return bad request response if title is not provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        contents: 'Updated contents',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if title is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: [],
        contents: 'Updated contents',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if title is longer than 50 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: '-'.repeat(51),
        contents: 'Updated contents',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be shorter than or equal to 50 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is not provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
        contents: [],
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is longer than 500 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
        contents: '-'.repeat(501),
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be shorter than or equal to 500 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no field is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({});

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no payload is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .put(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });
});

describe('PATCH /examples/users/:userId/notes/:noteId', () => {
  it('should update title', async () => {
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user, {
      title: 'Initial title',
      contents: 'Initial contents',
    });

    const updateResponse = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
      });
    expect(updateResponse.body).toEqual({
      success: true,
      data: {
        id: note.id,
        userId: user.id,
        title: 'Updated title',
        contents: 'Initial contents',
      },
    });
    expect(updateResponse.statusCode).toBe(HttpStatus.OK);

    const getResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(getResponse.body).toHaveProperty('data.title', 'Updated title');
    expect(getResponse.body).toHaveProperty(
      'data.contents',
      'Initial contents',
    );
  });

  it('should update contents', async () => {
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user, {
      title: 'Initial title',
      contents: 'Initial contents',
    });

    const updateResponse = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        contents: 'Updated contents',
      });
    expect(updateResponse.body).toEqual({
      success: true,
      data: {
        id: note.id,
        userId: user.id,
        title: 'Initial title',
        contents: 'Updated contents',
      },
    });
    expect(updateResponse.statusCode).toBe(HttpStatus.OK);

    const getResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(getResponse.body).toHaveProperty('data.title', 'Initial title');
    expect(getResponse.body).toHaveProperty(
      'data.contents',
      'Updated contents',
    );
  });

  it('should update title and contents with extra fields', async () => {
    const id = 1000000;
    const userId = 1000000;
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        id,
        userId,
        title: 'Updated title',
        contents: 'Updated contents',
      });

    expect(response.body).toHaveProperty('success', true);
    expect(response.body.data.id).not.toEqual(id);
    expect(response.body.data.userId).not.toEqual(userId);
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should return not found response if note does not exist', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: 'Updated title',
      });

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });

  it('should return bad request response if title is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: [],
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if title is longer than 50 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        title: '-'.repeat(51),
      });

    expect(response.body).toEqual({
      success: false,
      error: 'title must be shorter than or equal to 50 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is invalid', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        contents: [],
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if contents is longer than 500 characters', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({
        contents: '-'.repeat(501),
      });

    expect(response.body).toEqual({
      success: false,
      error: 'contents must be shorter than or equal to 500 characters',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no field is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token)
      .send({});

    expect(response.body).toEqual({
      success: false,
      error: 'at least one field required',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should return bad request response if no payload is provided', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .patch(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: false,
      error: 'at least one field required',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });
});

describe('DELETE /examples/users/:userId/notes/:noteId', () => {
  it('should delete note', async () => {
    const user = await createTestUserLogin(app);
    const note = await createTestNote(app, user, {
      title: 'Title',
      contents: 'Contents',
    });

    const deleteResponse = await request(app.getHttpServer())
      .delete(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(deleteResponse.body).toEqual({
      success: true,
      data: {
        id: note.id,
        userId: user.id,
        title: 'Title',
        contents: 'Contents',
      },
    });
    expect(deleteResponse.statusCode).toBe(HttpStatus.OK);

    const getResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}/notes/${note.id}`)
      .set('Authorization', 'Bearer ' + user.token);
    expect(getResponse.statusCode).toBe(HttpStatus.NOT_FOUND);
  });

  it('should return not found response if note does not exist', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .delete(`/examples/users/${user.id}/notes/1000`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });
});
