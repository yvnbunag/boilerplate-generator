import { AppModule } from '@/app.module';
import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { createTestUser } from 'test/lib/create-test-user-login';
import { TestService } from 'test/lib/test-service';
import { beforeEach, describe, expect, it } from 'vitest';

let app: INestApplication;

beforeEach(async () => {
  const { nestApp, testModule } = await createTestApp({
    imports: [AppModule],
    providers: [TestService],
  });

  await testModule.get<TestService>(TestService).seedDatabase();

  app = nestApp;
});

const email = 'test@example.com';
const password = 'password';

describe('POST /examples/auth/signup', () => {
  it('should sign up', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/signup')
      .send({ email, password });

    expect(response.body).toEqual({
      success: true,
      data: {
        id: expect.any(Number),
        email,
      },
    });
    expect(response.statusCode).toBe(HttpStatus.CREATED);
  });

  it('should fail sign up on invalid email', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/signup')
      .send({ email: 'invalid', password });

    expect(response.body).toEqual({
      success: false,
      error: 'email must be an email',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should fail sign up on invalid password', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/signup')
      .send({ email, password: '' });

    expect(response.body).toEqual({
      success: false,
      error: 'password should not be empty',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should fail sign up on invalid body', async () => {
    const response = await request(app.getHttpServer()).post(
      '/examples/auth/signup',
    );

    expect(response.body).toEqual({
      success: false,
      error: 'email must be an email',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should fail sign up on taken email', async () => {
    const initialResponse = await request(app.getHttpServer())
      .post('/examples/auth/signup')
      .send({ email, password });
    expect(initialResponse.statusCode).toBe(HttpStatus.CREATED);

    const failedResponse = await request(app.getHttpServer())
      .post('/examples/auth/signup')
      .send({ email, password });

    expect(failedResponse.body).toEqual({
      success: false,
      error: 'email already taken',
    });
    expect(failedResponse.statusCode).toBe(HttpStatus.CONFLICT);
  });
});

describe('POST /examples/auth/login', () => {
  it('should authenticate user', async () => {
    const credentials = await createTestUser(app);
    const response = await request(app.getHttpServer())
      .post('/examples/auth/login')
      .send(credentials);

    expect(response.body).toEqual({
      success: true,
      data: {
        token: expect.any(String),
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should fail on invalid email', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/login')
      .send({ email: 'invalid', password });

    expect(response.body).toEqual({
      success: false,
      error: 'email must be an email',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should fail on invalid password', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/login')
      .send({ email });

    expect(response.body).toEqual({
      success: false,
      error: 'password must be a string',
    });
    expect(response.statusCode).toBe(HttpStatus.BAD_REQUEST);
  });

  it('should fail on unauthorized credentials', async () => {
    const response = await request(app.getHttpServer())
      .post('/examples/auth/login')
      .send({ email, password });

    expect(response.body).toEqual({
      success: false,
      error: 'unauthorized',
    });
    expect(response.statusCode).toBe(HttpStatus.UNAUTHORIZED);
  });
});
