import { CreateNoteRequestDto } from '@/examples/notes/models/create-note-request-dto';
import { NoteResponseDto } from '@/examples/notes/models/note-response-dto';
import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';
import { TestUserLogin } from 'test/lib/create-test-user-login';

export async function createTestNote(
  app: INestApplication,
  user: TestUserLogin,
  note: Partial<CreateNoteRequestDto> = {},
): Promise<NoteResponseDto> {
  const payload: CreateNoteRequestDto = {
    title: 'Title',
    contents: 'Contents',
    ...note,
  };
  const response = await request(app.getHttpServer())
    .post(`/examples/users/${user.id}/notes`)
    .set('Authorization', 'Bearer ' + user.token)
    .send(payload);

  if (response.statusCode !== HttpStatus.CREATED) {
    throw new Error('failed to create test note');
  }

  return response.body.data;
}
