import { JwtUser } from '@/examples/auth/auth.types';

export function createTestJwtUser(jwtUser: Partial<JwtUser> = {}): JwtUser {
  return {
    userId: 0,
    email: 'test@example.com',
    roles: [],
    permissions: [],
    ...jwtUser,
  };
}
