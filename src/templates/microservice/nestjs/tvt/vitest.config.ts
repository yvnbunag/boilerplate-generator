import swc from 'unplugin-swc';
import tsconfigPaths from 'vite-tsconfig-paths';
import { defineConfig } from 'vitest/config';

const { TYPE = 'all' } = process.env;

process.env.NODE_ENV = 'test';

export default defineConfig({
  test: {
    include: [
      ...(TYPE === 'unit' || TYPE === 'all' ? ['**/*.spec.ts'] : []),
      ...(TYPE === 'integration' || TYPE === 'all' ? ['**/*.test.ts'] : []),
    ],
    globals: true,
    root: './',
  },
  plugins: [
    // This is required to build the test files with SWC
    swc.vite({
      // Explicitly set the module type to avoid inheriting this value from a `.swcrc` config file
      module: { type: 'es6' },
    }),
    tsconfigPaths(),
  ],
});
