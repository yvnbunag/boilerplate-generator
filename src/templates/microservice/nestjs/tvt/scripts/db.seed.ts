import { createDatasource } from '@/global/database/database.datasource';
import { seed } from '@/global/database/database.seed';

async function seedDatabase() {
  const dataSource = createDatasource();

  await dataSource.initialize();
  await seed(dataSource);
  await dataSource.destroy();
}

seedDatabase();
