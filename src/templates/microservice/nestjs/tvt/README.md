# <package-name>

A microservice project built with [NestJS](https://nestjs.com/), and powered by
[TypeORM](https://typeorm.io/), [Vitest](https://vitest.dev/) and [TypeScript](https://www.typescriptlang.org/).

<!-- omit in toc -->

## Contents

- [Requirements](#requirements)
- [Scaffold setup](#scaffold-setup)
- [Usage](#usage)
  - [Running tests](#running-tests)
  - [Development](#development)
  - [Production](#production)
  - [Linting code](#linting-code)
  - [Formatting code](#formatting-code)
- [Scaffold](#scaffold)

<br/>

## Requirements

1. [Node.js](https://nodejs.org/en/) version 18.15.0 or higher
2. [pnpm](https://pnpm.io/) version 8.3.1 or higher
   - May be swapped with other package managers such as
     [npm](https://docs.npmjs.com/) and [yarn](https://yarnpkg.com/)

<br/>

## Scaffold setup

1. Initialize git repository

```sh
git init
git add .
```

2. Install dependencies

```sh
pnpm install
```

3. Commit and push project to remote repository

```sh
git commit -m "<commit-message>"
git remote add origin <remote-origin>
git push --set-upstream origin <branch-name>
```

<br/>

## Usage

### Running tests

```sh
pnpm test

# Unit tests
pnpm test:unit

# Integration tests
pnpm test:integration
```

### Development

Set up environment variables

```sh
cp .env.example .env
```

Import [Postman](https://www.postman.com/) collection and environment

- [NestJS TVT.postman_collection.json](<NestJS TVT.postman_collection.json>)

- [NestJS TVT.postman_environment.json](<NestJS TVT.postman_environment.json>)

Start MySQL docker container

```sh
docker-compose up -d
```

Run database migration

```sh
pnpm db:migration:run
```

Seed database

```sh
pnpm db:seed
```

Start service in watched mode

```sh
pnpm dev
```

### Production

Build production package

```sh
pnpm build
```

Start service from production package

```sh
pnpm start
```

### Linting code

```sh
pnpm lint
```

### Formatting code

```sh
pnpm format
```

## Scaffold

Project scaffolded with [@ianbunag/scaffold](https://www.npmjs.com/package/@ianbunag/scaffold)
