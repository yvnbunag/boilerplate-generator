/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'NestJS',
  isDomain: true,
}

module.exports = config
