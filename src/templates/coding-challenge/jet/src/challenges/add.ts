/* eslint-disable no-console */

import { add } from '~/modules/add'

console.log(`5 + 5 = ${add(5, 5)}`)
console.log(`-5 + -5 = ${add(-5, -5)}`)
console.log(`10 + -5 = ${add(10, -5)}`)
