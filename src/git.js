const gitConfigPath = require('git-config-path')
const parseGitConfig = require('parse-git-config')
const extendShallow = require('extend-shallow')
const getUserName = require('git-user-name')
const { execSync } = require('child_process');

/**
 * Fork of https://www.npmjs.com/package/git-user-email that gets user email
 *  from current scope, patterned from
 *  https://www.npmjs.com/package/git-user-name
 */
function getUserEmail(options = {}) {
  const path = gitConfigPath(
    extendShallow(
      { type: 'global' },
      options && options.gitconfig
    )
  )
  const shallowOptions = extendShallow({ cwd: '/', path }, options)
  const config = parseGitConfig.sync(shallowOptions) || {}

  return config.user ? config.user.email : null
}

function runShellCommand(command) {
  try {
    const output = execSync(command, { encoding: 'utf8' });
    const firstLine = output.trim().split('\n')[0];
    return firstLine;
  } catch (error) {
    return null;
  }
}

module.exports = {
  getUserName: () => getUserName() || runShellCommand('git config user.name'),
  getUserEmail: () => getUserEmail() || runShellCommand('git config user.email'),
}
