#!/usr/bin/env node
require('module-alias').addAliases({ "~": __dirname })

const { readdir } = require('fs/promises')

const chalk = require('chalk')
const { copy, rename } = require('fs-extra')
const replace = require('replace')

const config = require('~/config')
const { promptText, selectPrompt } = require('~/prompt')
const { ScaffoldAbortedError } = require('~/errors')

/**
 * @typedef {import('@/template').Template} Template
 * @typedef {import('@/cli-prompt').CLIPrompt} CLIPrompt
 */

/**
 * Scan directory for its template definitions
 *
 * @param {string} base - Directory to scan for template definitions
 *
 * @returns {Promise<Template["Definitions"]>} Template definitions
 */
async function getTemplateDefinitions(base = `${__dirname}/templates`) {
  const domains = await readdir(base, { withFileTypes: true })
    .then((files) => files
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name)
    )
  const mappedDomains = domains.map((domain) => {
    const definition =  require(`${base}/${domain}/${config.file}`)

    return [domain, definition]
  })

  return Object.fromEntries(mappedDomains)
}

/**
 * @param {Template["Definitions"]} templateDefinitions
 *
 * @returns {CLIPrompt['Values']}
 */
function toSelectPromptValues(templateDefinitions) {
  const entries = Object.entries(templateDefinitions)
  const promptEntries = entries.map(([key, { display }]) => [key, display])

  return Object.fromEntries(promptEntries)
}

/**
 * Prompt for template to be scaffolded
 *
 * @param {string} base - Directory to scan for template definitions
 * @param {Array<Template['Aggregate']>} [domains]
 *
 * @returns {Promise<Array<Template['Aggregate']>>}
 */
async function promptTemplate(
  base = `${__dirname}/templates`,
  domains = []
) {
  const domainSeparator = ' > '
  const domain = domains
    .map(({ definition }) => definition.display)
    .join(domainSeparator)

  if (!domains.length) console.log('Select domain:')
  else console.log(domain)

  const templates = await getTemplateDefinitions(base)
  const template = await selectPrompt(toSelectPromptValues(templates))
  const definition = templates[template]
  const result = {
    name: template,
    domain,
    directory: `${base}/${template}`,
    definition
  }

  if (!definition.isDomain) {
    console.log(`${domain}${domainSeparator}${definition.display}`)

    return [...domains, result]
  }

  return await promptTemplate(`${base}/${template}`, [...domains, result])
}

/**
 * Prompt for template replacements
 *
 * @param {Template['Definition']['replacements']} [replacements] Replacements
 *  to be prompted for values
 *
 * @returns {Promise<Template['Definition']['replacements']>} Replacements with
 *  prompted values
 */
async function promptReplacements(replacements = []) {
  if (!replacements.length) return replacements

  const promptedReplacements = []

  for (const replacement of replacements) {
    const value = await promptText(
      replacement.display,
      {
        value: replacement.value,
        onCancel: ScaffoldAbortedError.throw
      }
    )

    promptedReplacements.push({
      ...replacement,
      value: value || replacement.value
    })
  }

  return promptedReplacements
}

/**
 * Scaffold project from template
 *
 * @param {string} template - Template directory
 * @param {string} target - Target directory
 * @param {Array<string>} excluded - Excluded files
 * @param {Array<[string, string]>} renames - Renamed files
 * @param {Template['Definition']['replacements']} [replacements] - Replacements
 */
async function scaffold(template, target, excluded, renames, replacements = []) {
  await copy(
    template,
    target,
    {
      filter: (currentFile)=> {
        return Boolean(excluded.find((file) => currentFile.indexOf(file) < 0))
      }
    }
  )

  for (const [renameSource, renameTarget] of renames) {
    await rename(
      `${target}/${renameSource}`,
      `${target}/${renameTarget}`
    ).catch(() => {})
  }

  for (const replacement of replacements) {
    replace({
      regex: replacement.pattern,
      replacement: replacement.value,
      paths: [target],
      recursive: true,
      silent: true,
    })
  }
}

/**
 *  Main program
 */
async function main() {
  try {
    const base = `${__dirname}/templates`
    const template = await promptTemplate(base)
      .then((result) => result.slice(-1).pop())
    const projectName = await promptText(
      'Project name',
      {
        required: true,
        onCancel: ScaffoldAbortedError.throw
      }
    )
    const replacements
      = await promptReplacements(template.definition.replacements)
    const targetDirectory = `${process.cwd()}/${projectName}`
    const defaultSuccessMessage = [
      chalk.underline(template.domain),
      'with',
      chalk.underline(template.definition.display),
      'template scaffolded in',
      targetDirectory
    ].join(' ')
    const successMessages = [
      defaultSuccessMessage,
      ...(template.definition.successMessages || [])
    ]

    await scaffold(
      template.directory,
      targetDirectory,
      config.excluded,
      config.renames,
      replacements
    )
    successMessages.forEach((message) => console.log(message))
  } catch (err) {
    if (err instanceof ScaffoldAbortedError) {
      console.log(chalk.red(err.message))
      process.exitCode = 1

      return
    }

    throw err
  }
}

main()
