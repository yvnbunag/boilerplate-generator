# Contributing

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Development](#development)
  - [Via package script](#via-package-script)
  - [Via global link](#via-global-link)
- [Templates directory](#templates-directory)
- [Scaffold configuration file](#scaffold-configuration-file)
  - [display](#display)
  - [isDomain](#isdomain)
  - [successMessages](#successmessages)
  - [replacements](#replacements)
- [Domain](#domain)
- [Template](#template)
- [Publishing and release](#publishing-and-release)

<br/>

## Requirements

1. [Node.js](https://nodejs.org/en/) version 16.11.1 or higher
2. [pnpm](https://pnpm.io/) version 6.11.0 or higher
   - May be swapped with other package managers such as
  [npm](https://docs.npmjs.com/) and [yarn](https://yarnpkg.com/)

<br/>

## Setup

Install dependencies

```sh
pnpm install
```

<br/>

## Development

Running the scaffold script locally

### Via package script

```sh
pnpm scaffold
```

### Via global link

Link local scaffold script globally

```sh
pnpm link --global
# OR
npm link
```

Run script

```sh
scaffold
```

Unlink global scaffold script

```sh
pnpm remove --global @ianbunag/scaffold
# OR
npm unlink @ianbunag/scaffold
npm remove --global @ianbunag/scaffold
```

<br/>

## Templates directory

Project templates are located here. The directory may be structured depending on
the need: An optional domain containing sub-domains directories or a project
template directory

<br/>

## Scaffold configuration file

Each domain or template directory's metadata are required to be defined in a
`scaffold.config.js` file, with the following properties:

### display

Display name of a domain / template

**Type:** `string`

**Required**

```js
// scaffold.config.js
module.exports = {
  display: 'Microservice (Back End)',
}
```

---

### isDomain

Marker if directory is a domain

**Type:** `boolean`

**Default:** `false`

```js
// scaffold.config.js
module.exports = {
  isDomain:  true,
}
```

---

### successMessages

Additional messages to print on successful project generation

**Type:** `Array<string>`

**Default:** `[]`

```js
// scaffold.config.js
module.exports = {
  successMessages: ['Verify package.json before starting development']
}
```

---

### replacements

Template placeholders to be prompted and replaced on scaffold. Each replacement
definition should have the following properties:

- **display** - Prompt display name
- **pattern** - Pattern to replace in template files
- **value** - Default value

**Type:** `Array<{ display: string, pattern: string, value: string }>`

**Default:** `[]`

```js
// scaffold.config.js
module.exports = {
  replacements: [{
    display: 'Package version',
    pattern: '<package-version>',
    value: '0.0.1'
  }]
}
```

File contents with matching pattern will then be replaced by default or provided
value on scaffold

```json
// package.json
{
  "version": "<package-version>",
}
```

## Domain

When a domain is selected during the scaffold prompt, the directory will be
scanned of its subdirectories which will then be prompted for selection, until a
template gets selected

To create a domain:

1. Create domain directory
2. Define scaffold configuration with `display` and `isDomain` set to `true`

```js
// src/templates/.../scaffold.config.js
/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'Microservice (Back End)',
  isDomain: true,
}

module.exports = config
```

<br/>

## Template

When a template gets selected:

1. The project name (target directory) is prompted
2. The configured replacements are prompted
3. The selected template directory is copied to the target directory
   1. `scaffold.config.js` excluded
4. All files in the target directory are modified with the configured
  replacements

To create a template:

1. Create template directory
2. Add template files
3. Define scaffold configuration with `display` and `isDomain` skipped or set to
  `false`
4. Configure `successMessages` and `replacements` as needed

```js
// src/templates/.../scaffold.config.js
const { standardReplacements } = require('~/config')

/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: "JET (Jest, ESLint and TypeScript)",
  successMessages: [
    'Verify package.json before starting development',
  ],
  replacements: standardReplacements,
}

module.exports = config
```

<br/>

## Publishing and release

To publish latest changes to the
[NPM repository](https://www.npmjs.com/package/@ianbunag/scaffold), create a tag
in the [GitLab repository](https://gitlab.com/ianbunag/scaffold/-/tags) for the
main branch. A release note must be included

On release, the remote main branch receives a commit with the following changes:

1. Package version set to the tag value
2. `README.md` file gets re-generated with absolute links to the
  [GitLab repository](https://gitlab.com/ianbunag/scaffold), for proper linking
  from the [NPM repository](https://www.npmjs.com/package/@ianbunag/scaffold)
