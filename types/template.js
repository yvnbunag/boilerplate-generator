/**
 * @typedef {Object} Replacement
 * @property {string} display
 * @property {string} pattern
 * @property {string} value
 *
 * @typedef {Object} Definition
 * @property {string} display
 * @property {boolean} [isDomain]
 * @property {Array<string>} [successMessages]
 * @property {Array<Replacement>} [replacements]
 *
 * @typedef {Object} Aggregate
 * @property {string} name
 * @property {string} domain
 * @property {string} directory
 * @property {Definition} definition
 *
 * @typedef {{ [key in string]: Definition }} Definitions
 *
 * @typedef {Object} Template
 * @property {Definition} Definition
 * @property {Definitions} Definitions
 * @property {Aggregate} Aggregate
 */

 module.exports = {}
