<!---
  This file is auto-generated during CI/CD. Make changes in README.local.md to
    ensure changes in this file are preserved on deploy.
-->

# @ianbunag/scaffold

[![npm version](https://img.shields.io/npm/v/@ianbunag/scaffold.svg?style=flat)](https://www.npmjs.com/package/@ianbunag/scaffold)
[![nodejs version](https://img.shields.io/node/v/@ianbunag/scaffold)](https://nodejs.org/en/)
[![Known Vulnerabilities](https://snyk.io/test/npm/@ianbunag/scaffold/badge.svg)](https://snyk.io/test/npm/@ianbunag/scaffold)
[![Continuous Delivery](https://img.shields.io/badge/dynamic/json?color=informational&label=%E2%9A%99%20CD&query=%24%5B0%5D.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F30410057%2Fpipelines%3Fscope%3Dtags%26per_page%3D1)](https://gitlab.com/ianbunag/scaffold/-/pipelines?page=1&scope=tags)
[![license](https://img.shields.io/npm/l/@ianbunag/scaffold)](./LICENSE)

CLI tool to quickly generate project from boilerplate

<br/>

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Installation](#installation)
  - [With pnpm](#with-pnpm)
  - [With yarn](#with-yarn)
  - [With npm](#with-npm)
- [Templates](#templates)
- [Usage](#usage)
- [Contributing](#contributing)

<br/>

## Requirements

1. [Node.js](https://nodejs.org/en/) version 16.11.1 or higher
2. [pnpm](https://pnpm.io/) version 6.11.0 or higher
   - May be swapped with other package managers such as
  [npm](https://docs.npmjs.com/) and [yarn](https://yarnpkg.com/)

<br/>

## Installation

### With pnpm

```sh
pnpm add --global @ianbunag/scaffold
```

### With yarn

```sh
yarn global add @ianbunag/scaffold
```

### With npm

```sh
npm install -g @ianbunag/scaffold
```

<br/>

## Templates

Listed are the currently available templates

- Coding challenge
  - [JET (Jest, ESLint and TypeScript)](./src/templates/coding-challenge/jet)
- Microservice (Back End)
  - Fastify
    - [JET (Jest, ESLint and TypeScript)](./src/templates/microservice/fastify/jet)
  - NestJS
    - [TVT (TypeORM, Vitest and TypeScript)](./src/templates/microservice/nestjs/tvt)
- Web (Front End)
  - Next.js
    - [MTC (Material UI, TypeScript and Cypress)](./src/templates/web/next-js/mtc)

<br/>

## Usage

1. Run `scaffold` command

```sh
scaffold
```

2. Select template

3. Provide project name, which would be the directory name

4. Provide template replacements if prompted

5. Navigate to scaffolded project

```sh
cd <project-name>
```

6. Preview scaffold `README.md` for setup instructions

![Scaffold](./documentation/scaffold.gif)

## Contributing

See [Contributing](./CONTRIBUTING.md)
